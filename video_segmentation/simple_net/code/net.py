import caffe

from caffe import layers as L, params as P
from caffe.coord_map import coord_map_from_to, crop
import numpy as np
import os, errno

def silentremove(filename):
    try:
        os.remove(filename)
    except OSError as e: # this would be "except OSError, e:" before Python 2.6
        if e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occured
        
def save_net(net_path, proto):
    with open(net_path, 'w') as f:
        f.write(proto)

def conv_relu(bottom, nout, ks=3, stride=1, pad=0, dilation=1, param_name=None, std=-1, filler_param = None, base_lr = 1, name_params = True):
    conv_param = {'kernel_size':ks}
    if stride != 1:
        conv_param['stride'] = stride
    if not hasattr(pad, '__len__'):
        if pad != 0:
            conv_param['pad'] = int(pad)
    else:
        if pad[0] != 0:
            conv_param['pad_w'] = int(pad[0])
        if pad[1] != 0:
            conv_param['pad_h'] = int(pad[1])
    if dilation != 1:
        conv_param['dilation'] = dilation
    else:
        conv_param['engine'] = P.Convolution.CUDNN
    
    conv_param['num_output'] = nout
    
    conv_param['param']= [dict(lr_mult=1 * base_lr, decay_mult=0 if base_lr == 0 else 1), dict(lr_mult=2 * base_lr, decay_mult=0)]
    if name_params and param_name is not None and param_name != '':
        conv_param['param'] = [dict(name=param_name + '_w', lr_mult=1 * base_lr, decay_mult=0 if base_lr == 0 else 1), dict(name=param_name + '_b', lr_mult=2*base_lr, decay_mult=0)]

    if std > 0:
      conv_param['weight_filler'] = dict(type='gaussian', std=std)
    
    if filler_param is not None:
        conv_param.update(filler_param.copy())

    conv = L.Convolution(bottom, **conv_param)
    return conv, L.ReLU(conv, in_place=True)

def max_pool(bottom, ks=2, stride=2):
    return L.Pooling(bottom, pool=P.Pooling.MAX, kernel_size=ks, stride=stride)


def conv_vgg(n, im, suffix='', last_layer_pad=0, first_layer_pad=0, add_extra_pad = True, use_dilated_net = True, add_pool5 = False, base_lr = 1, name_params = False):
    
    if add_extra_pad:
        pad = 1
    else:
        pad = 0

    conv, relu = conv_relu(im, 64, pad=pad + first_layer_pad, param_name='conv1_1', base_lr = base_lr, name_params=name_params)
    setattr(n, 'conv1_1' + suffix, conv)
    setattr(n, 'relu1_1' + suffix, relu)
    
    conv, relu = conv_relu(relu, 64, pad=pad, param_name='conv1_2', base_lr = base_lr, name_params=name_params)
    setattr(n, 'conv1_2' + suffix, conv)
    setattr(n, 'relu1_2' + suffix, relu)
    
    pool = max_pool(relu)
    setattr(n, 'pool1' + suffix, pool)
    
    
    
    conv, relu = conv_relu(pool, 128, pad=pad, param_name='conv2_1', base_lr = base_lr, name_params=name_params)
    setattr(n, 'conv2_1' + suffix, conv)
    setattr(n, 'relu2_1' + suffix, relu)
    
    conv, relu = conv_relu(relu, 128, pad=pad, param_name='conv2_2', base_lr = base_lr, name_params=name_params)
    setattr(n, 'conv2_2' + suffix, conv)
    setattr(n, 'relu2_2' + suffix, relu)
    
    pool = max_pool(relu)
    setattr(n, 'pool2' + suffix, pool)
    
    
    conv, relu = conv_relu(pool, 256, pad=pad, param_name='conv3_1', base_lr = base_lr, name_params=name_params)
    setattr(n, 'conv3_1' + suffix, conv)
    setattr(n, 'relu3_1' + suffix, relu)
    
    conv, relu = conv_relu(relu, 256, pad=pad, param_name='conv3_2', base_lr = base_lr, name_params=name_params)
    setattr(n, 'conv3_2' + suffix, conv)
    setattr(n, 'relu3_2' + suffix, relu)
    
    conv, relu = conv_relu(relu, 256, pad=pad, param_name='conv3_3', base_lr = base_lr, name_params=name_params)
    setattr(n, 'conv3_3' + suffix, conv)
    setattr(n, 'relu3_3' + suffix, relu)
    pool = max_pool(relu)
    setattr(n, 'pool3' + suffix, pool)
    

    conv, relu = conv_relu(pool, 512, pad=pad, param_name='conv4_1', base_lr = base_lr, name_params=name_params)
    setattr(n, 'conv4_1' + suffix, conv)
    setattr(n, 'relu4_1' + suffix, relu)
    
    conv, relu = conv_relu(relu, 512, pad=pad, param_name='conv4_2', base_lr = base_lr, name_params=name_params)
    setattr(n, 'conv4_2' + suffix, conv)
    setattr(n, 'relu4_2' + suffix, relu)
    
    conv, relu = conv_relu(relu, 512, pad=pad, param_name='conv4_3', base_lr = base_lr, name_params=name_params)
    setattr(n, 'conv4_3' + suffix, conv)
    setattr(n, 'relu4_3' + suffix, relu)


    if use_dilated_net:
        dilation = 2
        pool = relu
    else:
        dilation = 1
        pool = max_pool(relu)
        setattr(n, 'pool4' + suffix, pool)    
    
    conv, relu = conv_relu(pool, 512, pad=pad*dilation, dilation=dilation, param_name='conv5_1', base_lr = base_lr, name_params=name_params)
    setattr(n, 'conv5_1' + suffix, conv)
    setattr(n, 'relu5_1' + suffix, relu)
    
    conv, relu = conv_relu(relu, 512, pad=pad*dilation, dilation=dilation, param_name='conv5_2', base_lr = base_lr, name_params=name_params)
    setattr(n, 'conv5_2' + suffix, conv)
    setattr(n, 'relu5_2' + suffix, relu)
    
    conv, relu = conv_relu(relu, 512, pad=pad*dilation+last_layer_pad, dilation=dilation, param_name='conv5_3', base_lr = base_lr, name_params=name_params)
    setattr(n, 'conv5_3' + suffix, conv)
    setattr(n, 'relu5_3' + suffix, relu)
    
    
    if add_pool5:
        pool = max_pool(relu)
        setattr(n, 'pool5' + suffix, pool)
    
    return n

#Inputs: disp, next_im, pre_im
#Outputs: n.pdisp_suffix, n.pred_suffix
#Paremters: pred_num_out, conv_kernel_size, conv_num_out, disp_num_out


def match_unit(n, disp, next_im, pre_im, suffix, pred_num_out, conv_kernel_size, conv_num_out, predict_next_disp, disp_num_out, disp_upscale, filler_param = {}, base_lr = 1, engine=P.Convolution.CUDNN, flow_scale = 1.0):
    
    #crop pre_im and next_im with respect to disp
    next_imc = crop(next_im, disp)
    setattr(n, 'next_imc_' + suffix, next_imc)
    
    pre_imc = crop(pre_im, disp)
    setattr(n, 'pre_imc_' + suffix, pre_imc)
    
    #Apply convolution with conv_num_out and conv_kernel_size on next_imc
    next_conv = L.Convolution(next_imc, kernel_size=conv_kernel_size, pad = (conv_kernel_size - 1) / 2, param=[dict(lr_mult=1*base_lr, decay_mult=1), dict(lr_mult=2*base_lr, decay_mult=0)], num_output=conv_num_out, engine = engine, **filler_param)
    setattr(n, 'next_conv_' + suffix, next_conv)
  

    #Apply convolution with conv_num_out and conv_kernel_size on pre_imc
    pre_conv = L.Convolution(pre_imc, kernel_size=conv_kernel_size, pad = (conv_kernel_size - 1) / 2, param=[dict(lr_mult=1*base_lr, decay_mult=1), dict(lr_mult=2*base_lr, decay_mult=0)], num_output=conv_num_out, engine = engine, **filler_param)
    setattr(n, 'pre_conv_' + suffix, pre_conv)
    
    #Warp pre_conv
    pre_warped = L.FlowWarping(pre_conv, disp, flow_warping_param = dict(scale = flow_scale))
    setattr(n, 'pre_warped_' + suffix, pre_warped)
        
    #elmsum + Relu pre_warped and next_conv
    match = L.Eltwise(pre_warped, next_conv, eltwise_param=dict(operation=P.Eltwise.SUM))
    setattr(n, 'match_' + suffix, match)
    match_relu = L.ReLU(match, in_place=True)
    setattr(n, 'match_relu_' + suffix, match_relu)
    
    #Apply conv with pred_num_out and kernel_size = 1 to get matching predictions
    pred = L.Convolution(match_relu, kernel_size=1, param=[dict(lr_mult=1*base_lr, decay_mult=1), dict(lr_mult=2*base_lr, decay_mult=0)], num_output=pred_num_out, engine=engine, **filler_param)
    setattr(n, 'pred_' + suffix, pred)
    
    #Predict next displacement
    if predict_next_disp:
        #first non-linearity
        disp_conv, disp_relu = conv_relu(match_relu, disp_num_out, ks=1, filler_param = filler_param, base_lr=base_lr)
        setattr(n, 'disp_conv_' + suffix, disp_conv)
        setattr(n, 'disp_relu_' + suffix, disp_relu)
        
        #prediction
        adj_disp = L.Convolution(disp_relu, kernel_size=1, param=[dict(lr_mult=1*base_lr, decay_mult=1), dict(lr_mult=2*base_lr, decay_mult=0)], num_output=2, engine=engine, **filler_param)
        setattr(n, 'adj_disp_' + suffix, adj_disp)
        
        #pdisp = disp + adj_disp
        pdisp = L.Eltwise(disp, adj_disp, eltwise_param=dict(operation=P.Eltwise.SUM))
        setattr(n, 'pdisp_' + suffix, pdisp)
        
        #up-scale
        if disp_upscale > 1:
            up_pdisp = L.Deconvolution(pdisp, convolution_param=dict(kernel_size=4, stride=2, num_output=2, bias_term=False), param=dict(lr_mult=0, decay_mult= 0))
            setattr(n, 'up_pdisp_' + suffix, up_pdisp)

def fcn8_head(n, split, fc6_size = 4096, fc7_size = 4096, filler_param = {}, share_weights = False, vgg_lr = 1, matching_lr = 1, engine = P.Convolution.CUDNN, visible_flow = False, flow_scale = 1.0):
    conv_vgg(n, n.next_image, suffix='n', last_layer_pad=0, first_layer_pad=112, use_dilated_net=False, add_pool5=True, base_lr = vgg_lr, name_params = share_weights)
    conv_vgg(n, n.fg_image, suffix='m', last_layer_pad=0, first_layer_pad=112, use_dilated_net=False, add_pool5=True, base_lr = vgg_lr, name_params = share_weights)
    
    #compute n.pdisp_m5
    n.disp_m5_1 =  L.Convolution(n.pool5m, kernel_size=7, param=[dict(lr_mult=1 * matching_lr, decay_mult=1), dict(lr_mult=2 * matching_lr, decay_mult=0)], num_output=128, engine=engine,**filler_param)
    n.disp_n5_1 =  L.Convolution(n.pool5n, kernel_size=7, param=[dict(lr_mult=1 * matching_lr, decay_mult=1), dict(lr_mult=2 * matching_lr, decay_mult=0)], num_output=128, engine=engine, **filler_param)
    n.disp_5_1 = L.Eltwise(n.disp_m5_1, n.disp_n5_1, eltwise_param=dict(operation=P.Eltwise.SUM))
    n.disp_5_1_relu = L.ReLU(n.disp_5_1, in_place=True)
    n.disp_5_2, n.disp_5_2_relu = conv_relu(n.disp_5_1_relu, 128, ks=1, filler_param = filler_param, base_lr = matching_lr)
    n.disp_m5 = L.Convolution(n.disp_5_2_relu, kernel_size=1, param=[dict(lr_mult=1 * matching_lr, decay_mult=1), dict(lr_mult=2 * matching_lr, decay_mult=0)], num_output=2, engine=engine,**filler_param)
    
    if visible_flow:
        n.up32_disp_m5 = L.Deconvolution(n.disp_m5, convolution_param=dict(kernel_size=64, stride=32, num_output=2, bias_term=False), param=dict(lr_mult=0, decay_mult= 0))
        n.visible_disp_m5 = crop(n.up32_disp_m5, n.next_image)
    #
    
    n.fc6m =  L.Convolution(n.pool5m, kernel_size=7, param=[dict(lr_mult=1 * matching_lr, decay_mult=1), dict(lr_mult=2 * matching_lr, decay_mult=0)], num_output=fc6_size, engine=engine,**filler_param)
    n.fc6m_warped = L.FlowWarping(n.fc6m, n.disp_m5, flow_warping_param = dict(scale = flow_scale))
    n.fc6n =  L.Convolution(n.pool5n, kernel_size=7, param=[dict(lr_mult=1 * matching_lr, decay_mult=1), dict(lr_mult=2 * matching_lr, decay_mult=0)], num_output=fc6_size, engine=engine, **filler_param)
    n.fc6 = L.Eltwise(n.fc6m_warped, n.fc6n, eltwise_param=dict(operation=P.Eltwise.SUM))
    n.relu6 = L.ReLU(n.fc6, in_place=True)
    
    if split == 'train':
        n.drop6 = L.Dropout(n.relu6, dropout_ratio=0.5, in_place=True)
        n.fc7, n.relu7 = conv_relu(n.drop6, fc7_size, ks=1, filler_param = filler_param, base_lr = matching_lr)
        n.drop7 = L.Dropout(n.relu7, dropout_ratio=0.5, in_place=True)    
    else:
        n.fc7, n.relu7 = conv_relu(n.relu6, fc7_size, ks=1, filler_param = filler_param, base_lr = matching_lr)

    n.score_fc = L.Convolution(n.relu7, kernel_size=1, param=[dict(lr_mult=1*matching_lr, decay_mult=1), dict(lr_mult=2*matching_lr, decay_mult=0)], num_output=21, engine=engine, **filler_param)

    
    
def match_net(n, split, cur_shape = None, next_shape = None, fc6_size = 4096, fc7_size = 4096, nrefinemnet_unit= 0, filler_param = {}, share_weights = False, engine = P.Convolution.CUDNN, visible_flow = False, flow_scale = 1.0, compute_obj_flow = False):
    
    #n.silence_bg_im = L.Silence(n.bg_image, ntop=0)
    #n.silence_cur_mask = L.Silence(n.current_mask, ntop=0)
    #n.silence_cur = L.Silence(n.current_image, ntop=0)
    
    #Create network
    if cur_shape.shape != next_shape.shape:
        raise Exception('Current and next image should have the same size in match net architecture')
    
    if nrefinemnet_unit == 0:
        vgg_lr = 0
        matching1_lr = 1
    elif nrefinemnet_unit > 0:
        vgg_lr = 0
        matching1_lr = 1
        matching2_lr = 1
        matching3_lr = 1
    
    
    fcn8_head(n, split, fc6_size, fc7_size, filler_param, share_weights = share_weights, vgg_lr = vgg_lr, matching_lr = matching1_lr, visible_flow = visible_flow, flow_scale = 1/32.0/flow_scale)
    
    if nrefinemnet_unit > 0:
        n.upscore2_fc = L.Deconvolution(n.score_fc, convolution_param=dict(kernel_size=4, stride=2, num_output=21, bias_term=False), param=dict(lr_mult=0,decay_mult=0))
        #Compute n.up2_disp_m5
        n.up2_disp_m5 = L.Deconvolution(n.disp_m5, convolution_param=dict(kernel_size=4, stride=2, num_output=2, bias_term=False), param=dict(lr_mult=0,decay_mult=0))
        

        #Outputs:  n.pred_m4, n.pdisp_m4
        n.scale_pool4n = L.Scale(n.pool4n, filler=dict(type='constant',value=0.01), param=[dict(lr_mult=0,decay_mult=0)])
        n.scale_pool4m = L.Scale(n.pool4m, filler=dict(type='constant',value=0.01), param=[dict(lr_mult=0,decay_mult=0)])
        match_unit(n, n.up2_disp_m5, n.scale_pool4n, n.scale_pool4m, 'm4', pred_num_out = 21, conv_kernel_size = 3, conv_num_out = 128, predict_next_disp = (nrefinemnet_unit > 1), disp_num_out = 128, disp_upscale = 2, filler_param = filler_param, base_lr = matching2_lr, flow_scale = 1/16.0/flow_scale)
        n.pred_m4c = crop(n.pred_m4, n.upscore2_fc)
        n.fuse_m4 = L.Eltwise(n.upscore2_fc, n.pred_m4c, eltwise_param=dict(operation=P.Eltwise.SUM))
        if nrefinemnet_unit == 1:
            n.upscore16 = L.Deconvolution(n.fuse_m4, convolution_param=dict(kernel_size=32, stride=16, num_output=21 , bias_term=False), param=dict(lr_mult=0,decay_mult=0))
            n.score16c = crop(n.upscore16, n.next_image)
            n.score = L.Convolution(n.score16c, kernel_size=1, param=[dict(lr_mult=1*matching2_lr, decay_mult=1), dict(lr_mult=2*matching2_lr, decay_mult=0)], num_output=2, engine=engine, **filler_param)
            return
        
        if visible_flow:
            n.up16_disp_m4 = L.Deconvolution(n.pdisp_m4, convolution_param=dict(kernel_size=32, stride=16, num_output=2, bias_term=False), param=dict(lr_mult=0,decay_mult=0))
            n.visible_disp_m4 = crop(n.up16_disp_m4, n.next_image)
        
        n.upscore_m4 = L.Deconvolution(n.fuse_m4, convolution_param=dict(kernel_size=4, stride=2, num_output=21, bias_term=False), param=dict(lr_mult=0,decay_mult=0))
        #Outputs:  n.pred_m3, n.pdisp_m3
        n.scale_pool3n = L.Scale(n.pool3n, filler=dict(type='constant',value=0.0001), param=[dict(lr_mult=0,decay_mult=0)])
        n.scale_pool3m = L.Scale(n.pool3m, filler=dict(type='constant',value=0.0001), param=[dict(lr_mult=0,decay_mult=0)])
        match_unit(n, n.up_pdisp_m4, n.scale_pool3n, n.scale_pool3m, 'm3', pred_num_out = 21, conv_kernel_size = 3, conv_num_out = 128, predict_next_disp = False, disp_num_out = 128, disp_upscale = 2, filler_param = filler_param, base_lr = matching3_lr, flow_scale = 1/8.0/flow_scale)
        n.pred_m3c = crop(n.pred_m3, n.upscore_m4)
        n.fuse_m3 = L.Eltwise(n.upscore_m4, n.pred_m3c, eltwise_param=dict(operation=P.Eltwise.SUM))
        
        n.uppred_m3 = L.Deconvolution(n.fuse_m3, convolution_param=dict(kernel_size=16, stride=8, num_output=21, bias_term=False), param=dict(lr_mult=0,decay_mult=0))
        n.pred_m3c = crop(n.uppred_m3, n.next_image)
        
        n.score = L.Convolution(n.pred_m3c, kernel_size=1, param=[dict(lr_mult=1*matching3_lr, decay_mult=1), dict(lr_mult=2*matching3_lr, decay_mult=0)], num_output=2, engine=engine, **filler_param)
    else:
        n.upscore2 = L.Deconvolution(n.score_fc, convolution_param=dict(kernel_size=64, stride=32, num_output=21, bias_term=False), param=dict(lr_mult=0,decay_mult=0))
        n.score32c = crop(n.upscore2, n.next_image)
        n.score = L.Convolution(n.score32c, kernel_size=1, param=[dict(lr_mult=1*matching1_lr, decay_mult=1), dict(lr_mult=2*matching1_lr, decay_mult=0)], num_output=2, engine=engine, **filler_param)
        
        
    if compute_obj_flow:
        assert nrefinemnet_unit == 2
        n.upobj_flow = L.Deconvolution(n.up_pdisp_m4, convolution_param=dict(kernel_size=16, stride=8, num_output=2, bias_term=False), param=dict(lr_mult=0,decay_mult=0))
        n.obj_flow = crop(n.upobj_flow, n.next_image)
        

# bg_method = 'FG', 'FG/BG', 'MASK'
def epicmatch_net(n, split, cur_shape = None, next_shape = None, fc6_size = 4096, fc7_size = 4096, nrefinemnet_unit= 0, filler_param = {}, share_weights = False, engine = P.Convolution.CUDNN, flow_params = [], bg_method='FG'):
    
    nout_prediction = 21
    nout_match = 64
    disp_params = [None] * len(flow_params)
    first_layer_pad = 112
    #Create network
    if cur_shape.shape != next_shape.shape:
        raise Exception('Current and next image should have the same size in match net architecture')
    
    if nrefinemnet_unit == 0:
        vgg_lr = 0
        matching1_lr = 1
    elif nrefinemnet_unit > 0:
        vgg_lr = 1
        matching1_lr = 1
        matching_lrs = [1, 1, 1, 1]
    
    
    conv_vgg(n, n.next_image, suffix='n', last_layer_pad=0, first_layer_pad=first_layer_pad, use_dilated_net=False, add_pool5=True, base_lr = vgg_lr, name_params = share_weights)
    
    if bg_method == 'FG':
        conv_vgg(n, n.fg_image, suffix='m', last_layer_pad=0, first_layer_pad=first_layer_pad, use_dilated_net=False, add_pool5=True, base_lr = vgg_lr, name_params = share_weights)
        warp_crop_ref = n.fg_image
            #n.silence_cur = L.Silence(n.current_image, ntop=0)
        #n.silence_bg_im = L.Silence(n.bg_image, ntop=0)
        #n.silence_cur_mask = L.Silence(n.current_mask, ntop=0)
    elif bg_method == 'FG/BG':
        conv_vgg(n, n.fg_image, suffix='m', last_layer_pad=0, first_layer_pad=first_layer_pad, use_dilated_net=False, add_pool5=True, base_lr = vgg_lr, name_params = share_weights)
        conv_vgg(n, n.bg_image, suffix='b', last_layer_pad=0, first_layer_pad=first_layer_pad, use_dilated_net=False, add_pool5=True, base_lr = vgg_lr, name_params = share_weights)
        warp_crop_ref = n.fg_image
            #n.silence_cur = L.Silence(n.current_image, ntop=0)
            #n.silence_cur_mask = L.Silence(n.current_mask, ntop=0)
    elif bg_method == 'MASK':    
        n.cur_im_mask = L.Concat(n.current_image, n.current_mask)
        conv_vgg(n, n.cur_im_mask, suffix='m', last_layer_pad=0, first_layer_pad=first_layer_pad, use_dilated_net=False, add_pool5=True, base_lr = vgg_lr, name_params = share_weights)
        warp_crop_ref = n.cur_im_mask
        #n.silence_bg_im = L.Silence(n.bg_image, ntop=0)
        #n.silence_fg_im = L.Silence(n.fg_image, ntop=0)
    
    
    n.fc6m =  L.Convolution(n.pool5m, kernel_size=7, param=[dict(lr_mult=1 * matching1_lr, decay_mult=0 if matching1_lr == 0 else 1), dict(lr_mult=2 * matching1_lr, decay_mult=0)], num_output=fc6_size, engine=engine,**filler_param)
    if bg_method == 'FG/BG':
        n.fc6b =  L.Convolution(n.pool5b, kernel_size=7, param=[dict(lr_mult=1 * matching1_lr, decay_mult=0 if matching1_lr == 0 else 1), dict(lr_mult=2 * matching1_lr, decay_mult=0)], num_output=fc6_size, engine=engine,**filler_param)
    if len(flow_params) > 0 and flow_params[0] is not None:
        disp = getattr(n, flow_params[0][0])
        flow_scale = flow_params[0][3]
        #name, down_scale, offset, flow_scale
        ax, down_scale, offset = coord_map_from_to(n.fc6m, warp_crop_ref)
        disp_params[0] = (flow_params[0][0], down_scale[0], offset[0], flow_scale)
        n.fc6m_warped = L.FlowWarping(n.fc6m, disp, flow_warping_param = dict(scale = flow_scale))
        pre_warped = n.fc6m_warped
        if bg_method == 'FG/BG':
            n.fc6b_warped = L.FlowWarping(n.fc6b, disp, flow_warping_param = dict(scale = flow_scale))
            bg_warped = n.fc6b_warped
    else:
        pre_warped = n.fc6m
        if bg_method == 'FG/BG':
            bg_warped = n.fc6b

    n.fc6n =  L.Convolution(n.pool5n, kernel_size=7, param=[dict(lr_mult=1 * matching1_lr, decay_mult=0 if matching1_lr == 0 else 1), dict(lr_mult=2 * matching1_lr, decay_mult=0)], num_output=fc6_size, engine=engine, **filler_param)
    if bg_method == 'FG/BG':
        n.fc6 = L.Eltwise(pre_warped, bg_warped, n.fc6n, eltwise_param=dict(operation=P.Eltwise.SUM))
    else:
        n.fc6 = L.Eltwise(pre_warped, n.fc6n, eltwise_param=dict(operation=P.Eltwise.SUM))
    n.relu6 = L.ReLU(n.fc6, in_place=True)
    
    if split == 'train':
        n.drop6 = L.Dropout(n.relu6, dropout_ratio=0.5, in_place=True)
        n.fc7, n.relu7 = conv_relu(n.drop6, fc7_size, ks=1, filler_param = filler_param, base_lr = matching1_lr)
        n.drop7 = L.Dropout(n.relu7, dropout_ratio=0.5, in_place=True)    
    else:
        n.fc7, n.relu7 = conv_relu(n.relu6, fc7_size, ks=1, filler_param = filler_param, base_lr = matching1_lr)

    n.score_fc = L.Convolution(n.relu7, kernel_size=1, param=[dict(lr_mult=1*matching1_lr, decay_mult=0 if matching1_lr == 0 else 1), dict(lr_mult=2*matching1_lr, decay_mult=0)], num_output=nout_prediction, engine=engine, **filler_param)
    
    #assert (nrefinemnet_unit + 1) == len(flow_params)
    if nrefinemnet_unit == 0:
        n.upscore32 = L.Deconvolution(n.score_fc, convolution_param=dict(kernel_size=64, stride=32, num_output=21, bias_term=False), param=dict(lr_mult=0,decay_mult=0))
        n.score32c = crop(n.upscore32, n.next_image)
        n.score = L.Convolution(n.score32c, kernel_size=1, param=[dict(lr_mult=1*matching1_lr, decay_mult=0 if matching1_lr == 0 else 1), dict(lr_mult=2*matching1_lr, decay_mult=0)], num_output=2, engine=engine, **filler_param)
        return disp_params
    
    n.upscore2_fc = L.Deconvolution(n.score_fc, convolution_param=dict(kernel_size=4, stride=2, num_output=nout_prediction, bias_term=False), param=dict(lr_mult=0,decay_mult=0))
    skip_from = ['pool4', 'pool3', 'pool2', 'pool1']
    skip_scale = [.01, .01, .0001, .0001]
    prev_prediction_name = 'upscore2_fc'
    
    for ref_id in range(0, nrefinemnet_unit):
        #Apply convolution with conv_num_out and conv_kernel_size on next_image
        skip_ntop = getattr(n, skip_from[ref_id] + 'n')
        if skip_scale[ref_id] != 1.0:
            skip_ntop = L.Scale(skip_ntop, filler=dict(type='constant',value=skip_scale[ref_id]), param=[dict(lr_mult=0,decay_mult=0)])
            setattr(n, skip_from[ref_id] + 'n_scale', skip_ntop)

        next_conv_1 = L.Convolution(skip_ntop, kernel_size=3, pad=1, param=[dict(lr_mult=1*matching_lrs[ref_id], decay_mult=0 if matching_lrs[ref_id] == 0 else 1), dict(lr_mult=2*matching_lrs[ref_id], decay_mult=0)], num_output=nout_match, engine = engine, **filler_param)
        setattr(n, 'skip' + str(4 - ref_id) + 'n_conv_1', next_conv_1)
        
        #Apply convolution with conv_num_out and conv_kernel_size on pre_im
        skip_mtop = getattr(n, skip_from[ref_id] + 'm')
        if skip_scale[ref_id] != 1.0:
            skip_mtop = L.Scale(skip_mtop, filler=dict(type='constant',value=skip_scale[ref_id]), param=[dict(lr_mult=0,decay_mult=0)])
            setattr(n, skip_from[ref_id] + 'm_scale', skip_mtop)

        pre_conv_1 = L.Convolution(skip_mtop, kernel_size=3, pad=1, param=[dict(lr_mult=1*matching_lrs[ref_id], decay_mult=0 if matching_lrs[ref_id] == 0 else 1), dict(lr_mult=2*matching_lrs[ref_id], decay_mult=0)], num_output=nout_match, engine = engine, **filler_param)
        setattr(n, 'skip' + str(4 - ref_id) + 'm_conv_1', pre_conv_1)
        
        if bg_method == 'FG/BG':
            #Apply convolution with conv_num_out and conv_kernel_size on pre_im
            skip_btop = getattr(n, skip_from[ref_id] + 'b')
            if skip_scale[ref_id] != 1.0:
                skip_btop = L.Scale(skip_btop, filler=dict(type='constant',value=skip_scale[ref_id]), param=[dict(lr_mult=0,decay_mult=0)])
                setattr(n, skip_from[ref_id] + 'b_scale', skip_btop)
            bg_conv_1 = L.Convolution(skip_btop, kernel_size=3, pad=1, param=[dict(lr_mult=1*matching_lrs[ref_id], decay_mult=0 if matching_lrs[ref_id] == 0 else 1), dict(lr_mult=2*matching_lrs[ref_id], decay_mult=0)], num_output=nout_match, engine = engine, **filler_param)
            setattr(n, 'skip' + str(4 - ref_id) + 'b_conv_1', bg_conv_1)
   
        dscale_factor = 2**(4-ref_id)
        #Warp pre_conv_1 and bg_conv_1
        if len(flow_params) > (ref_id + 1) and flow_params[ref_id+1] is not None:    
            disp = getattr(n, flow_params[ref_id + 1][0])
            assert dscale_factor == flow_params[ref_id + 1][1]
            flow_scale = flow_params[ref_id + 1][3]
            ax, down_scale, offset = coord_map_from_to(pre_conv_1, warp_crop_ref)
            disp_params[ref_id + 1] = (flow_params[ref_id + 1][0], down_scale[0], offset[0], flow_scale)
            
            pre_warped = L.FlowWarping(pre_conv_1, disp, flow_warping_param = dict(scale = flow_scale))
            setattr(n, 'skip' + str(4 - ref_id) + 'n_warped', pre_warped)
            
            if bg_method == 'FG/BG':
                bg_warped = L.FlowWarping(bg_conv_1, disp, flow_warping_param = dict(scale = flow_scale))
                setattr(n, 'skip' + str(4 - ref_id) + 'bg_warped', bg_warped)
        else:
            pre_warped = pre_conv_1
            #setattr(n, 'skip' + str(4 - ref_id) + 'n_warped', pre_conv_1)
            
            if bg_method == 'FG/BG':
                bg_warped = bg_conv_1
                #setattr(n, 'skip' + str(4 - ref_id) + 'bg_warped', bg_conv_1)
                
        #elmsum + Relu bg_warped, pre_warped, and next_conv_1
        if bg_method == 'FG/BG':
            match = L.Eltwise(pre_warped, bg_warped, next_conv_1, eltwise_param=dict(operation=P.Eltwise.SUM))
        else:
            match = L.Eltwise(pre_warped, next_conv_1, eltwise_param=dict(operation=P.Eltwise.SUM))
        setattr(n, 'skip' + str(4 - ref_id) + '_conv_1', match)
        match_relu = L.ReLU(match, in_place=True)
        setattr(n, 'skip' + str(4 - ref_id) + '_relu_1', match_relu)
    
    
        #Apply conv+relu to the matching result
        match2_conv, match2_relu = conv_relu(match_relu, nout_prediction, ks=3, pad=1, filler_param = filler_param, base_lr=matching_lrs[ref_id])
        setattr(n, 'skip' + str(4 - ref_id) + '_conv_2', match2_conv)
        setattr(n, 'skip' + str(4 - ref_id) + '_relu_2', match2_relu)
                
        #Merge match2_relu and last_prediction
        prev_prediction = getattr(n, prev_prediction_name)
        match3_conv = L.Convolution(match2_relu, kernel_size=3, pad=1, param=[dict(lr_mult=1*matching_lrs[ref_id], decay_mult=0 if matching_lrs[ref_id] == 0 else 1), dict(lr_mult=2*matching_lrs[ref_id], decay_mult=0)], num_output=nout_prediction, engine = engine, **filler_param)
        setattr(n, 'skip' + str(4 - ref_id) + '_conv_3', match3_conv)
        match3c_conv = crop(match3_conv, prev_prediction)
        setattr(n, 'skip' + str(4 - ref_id) + 'c_conv_3', match3c_conv)
                
                
        prev_prediction_conv = L.Convolution(prev_prediction, kernel_size=3, pad=1, param=[dict(lr_mult=1*matching_lrs[ref_id], decay_mult=0 if matching_lrs[ref_id] == 0 else 1), dict(lr_mult=2*matching_lrs[ref_id], decay_mult=0)], num_output=nout_prediction, engine = engine, **filler_param)
        setattr(n, 'skip' + str(4 - ref_id) + '_conv_4', prev_prediction_conv)
                
                
        pred_conv = L.Eltwise(prev_prediction_conv, match3c_conv, eltwise_param=dict(operation=P.Eltwise.SUM))
        pred = L.ReLU(pred_conv, in_place=True)
        setattr(n, 'pred_' + str(4 - ref_id) + '_conv', pred_conv)
        setattr(n, 'pred_' + str(4 - ref_id), pred)
                
        if ref_id < nrefinemnet_unit - 1:
            #upsample
            uppred = L.Deconvolution(pred, convolution_param=dict(kernel_size=4, stride=2, num_output=nout_prediction, bias_term=False), param=dict(lr_mult=0,decay_mult=0))
            setattr(n, 'uppredx2_' + str(4 - ref_id), uppred)
            prev_prediction_name = 'uppredx2_' + str(4 - ref_id)
        else:
            uppred = L.Deconvolution(pred, convolution_param=dict(kernel_size=2*dscale_factor, stride=dscale_factor, num_output=nout_prediction, bias_term=False), param=dict(lr_mult=0,decay_mult=0))
            setattr(n, 'uppred_' + str(4 - ref_id), uppred)
            uppredc = crop(uppred, n.next_image)
            setattr(n, 'uppredc_' + str(4 - ref_id), uppredc)
            n.score = L.Convolution(uppredc, kernel_size=1, param=[dict(lr_mult=1*matching_lrs[ref_id], decay_mult=0 if matching_lrs[ref_id] == 0 else 1), dict(lr_mult=2*matching_lrs[ref_id], decay_mult=0)], num_output=2, engine=engine, **filler_param)
    return disp_params

def fcn8_net(n, split, cur_shape = None, next_shape = None, fc6_size = 4096, fc7_size = 4096, crop_params = None, initialize_fcs=False):     
    return_crop_params = True if crop_params is None else False
    #Create network
    if cur_shape is None or next_shape is None:
        concat_pad = np.zeros((2,))
    else:
      concat_pad = (next_shape - cur_shape)/2.0/16.0

    if not all(concat_pad == np.round(concat_pad)):
        raise Exception

    conv_vgg(n, n.next_image, suffix='n', last_layer_pad=0, first_layer_pad=112, use_dilated_net=False, add_pool5=True)
    
    # concatination
    if crop_params is not None:
        conv_vgg(n, n.fg_image, suffix='m', last_layer_pad=concat_pad, first_layer_pad=112, use_dilated_net=False, add_pool5=True)
        #n.silence_cur = L.Silence(n.current_image, ntop=0)
        n.concat1 = L.Concat(n.pool5m, n.pool5n)
        vgg_out = n.concat1
    else:
        vgg_out = n.pool5n
    
    # fully conv
    if initialize_fcs:
        std = .01
    else:
        std = -1
        
    n.fc6, n.relu6 = conv_relu(vgg_out, fc6_size, ks=7, std=std)  
    if split == 'train':
        n.drop6 = L.Dropout(n.relu6, dropout_ratio=0.5, in_place=True)
        n.fc7, n.relu7 = conv_relu(n.drop6, fc7_size, ks=1, std=std)
        n.drop7 = L.Dropout(n.relu7, dropout_ratio=0.5, in_place=True)    
    else:
        n.fc7, n.relu7 = conv_relu(n.relu6, fc7_size, ks=1)
    
    if initialize_fcs:
        n.score_fc = L.Convolution(n.relu7, kernel_size=1, param=[dict(lr_mult=1, decay_mult=1), dict(lr_mult=2, decay_mult=0)], weight_filler=dict(type='gaussian', std=std), num_output=21)
    else:
        n.score_fc = L.Convolution(n.relu7, kernel_size=1, param=[dict(lr_mult=1, decay_mult=1), dict(lr_mult=2, decay_mult=0)], num_output=21)
        
    n.upscore2 = L.Deconvolution(n.score_fc, convolution_param=dict(kernel_size=4, stride=2, num_output=21), param=dict(lr_mult=0,decay_mult=0))
    n.score_pool4 = L.Convolution(n.pool4n, kernel_size=1, param=[dict(lr_mult=1, decay_mult=1), dict(lr_mult=2, decay_mult=0)], num_output=21)
    if return_crop_params:
        crop_params = [None] * 3
        ax, a, b = coord_map_from_to(n.score_pool4, n.upscore2)
        assert (a == 1).all(), 'scale mismatch on crop (a = {})'.format(a)
        assert (b <= 0).all(), 'cannot crop negative offset (b = {})'.format(b)
        assert (np.round(b) == b).all(), 'cannot crop noninteger offset (b = {})'.format(b)
        crop_params[0] = dict(axis=ax + 1,  # +1 for first cropping dim.
                                  offset=list(-np.round(b).astype(int)))
    
    n.score_pool4c = L.Crop(n.score_pool4, n.upscore2, crop_param=crop_params[0])
    n.fuse_pool4 = L.Eltwise(n.upscore2, n.score_pool4c, eltwise_param=dict(operation=P.Eltwise.SUM))
    
    
    
    n.upscore_pool4 = L.Deconvolution(n.fuse_pool4, convolution_param=dict(kernel_size=4, stride=2, num_output=21, bias_term=False), param=dict(lr_mult=0,decay_mult=0))
    n.score_pool3 = L.Convolution(n.pool3n, kernel_size=1, param=[dict(lr_mult=1, decay_mult=1), dict(lr_mult=2, decay_mult=0)], num_output=21)
    
    if return_crop_params:
        ax, a, b = coord_map_from_to(n.score_pool3, n.upscore_pool4)
        assert (a == 1).all(), 'scale mismatch on crop (a = {})'.format(a)
        assert (b <= 0).all(), 'cannot crop negative offset (b = {})'.format(b)
        assert (np.round(b) == b).all(), 'cannot crop noninteger offset (b = {})'.format(b)
        crop_params[1] = dict(axis=ax + 1,  # +1 for first cropping dim.
                                  offset=list(-np.round(b).astype(int)))
    
    n.score_pool3c = L.Crop(n.score_pool3, n.upscore_pool4, crop_param=crop_params[1])
    n.fuse_pool3 = L.Eltwise(n.upscore_pool4, n.score_pool3c, eltwise_param=dict(operation=P.Eltwise.SUM))
    
    
    n.upscore8 = L.Deconvolution(n.fuse_pool3, convolution_param=dict(kernel_size=16, stride=8, num_output=21, bias_term=False), param=dict(lr_mult=0,decay_mult=0))
    if return_crop_params:
        ax, a, b = coord_map_from_to(n.upscore8, n.next_image)
        assert (a == 1).all(), 'scale mismatch on crop (a = {})'.format(a)
        assert (b <= 0).all(), 'cannot crop negative offset (b = {})'.format(b)
        assert (np.round(b) == b).all(), 'cannot crop noninteger offset (b = {})'.format(b)
        crop_params[2] = dict(axis=ax + 1,  # +1 for first cropping dim.
                                  offset=list(-np.round(b).astype(int)))
    
    n.score_pool3c = L.Crop(n.upscore8, n.next_image, crop_param=crop_params[2])
    
    if initialize_fcs:
        n.score = L.Convolution(n.score_pool3c, kernel_size=1, param=[dict(lr_mult=10, decay_mult=1), dict(lr_mult=20, decay_mult=0)], weight_filler=dict(type='gaussian', std=std), num_output=2)
    else:
        n.score = L.Convolution(n.score_pool3c, kernel_size=1, param=[dict(lr_mult=10, decay_mult=1), dict(lr_mult=20, decay_mult=0)], num_output=2)
    return crop_params if return_crop_params else n


def simple_net(n, split, fc6_size = 4096, fc7_size = 4096, initialize_fcs=False, cur_shape = None, next_shape = None):
  
    #Get crop layer parameters
    tmp_net = caffe.NetSpec()
    tmp_net.im, tmp_net.label = L.MemoryData(batch_size=1, channels=3, height=244, width=244, ntop=2)
    conv_vgg(tmp_net, tmp_net.im, suffix='', last_layer_pad=0, first_layer_pad=112)
    tmp_net.fc6, tmp_net.relu6 = conv_relu(tmp_net.conv5_3, 4096, ks=7, dilation=4)        
    tmp_net.fc7, tmp_net.relu7 = conv_relu(tmp_net.relu6, 4096, ks=1, pad=0)
    tmp_net.fc8 = L.Convolution(tmp_net.relu7, kernel_size=1, num_output=2)
    tmp_net.upscore = L.Deconvolution(tmp_net.fc8, convolution_param=dict(kernel_size=16, stride=8, num_output=2))
    
    ax, a, b = coord_map_from_to(tmp_net.upscore, tmp_net.im)
    assert (a == 1).all(), 'scale mismatch on crop (a = {})'.format(a)
    assert (b <= 0).all(), 'cannot crop negative offset (b = {})'.format(b)
    assert (np.round(b) == b).all(), 'cannot crop noninteger offset (b = {})'.format(b)
    #
    
    #Create network
    if cur_shape is None or next_shape is None:
        concat_pad = np.zeros((2,))
    else:
      concat_pad = (next_shape - cur_shape)/2.0/8.0
    if not all(concat_pad == np.round(concat_pad)):
        raise Exception

    

    conv_vgg(n, n.current_image, suffix='c', last_layer_pad=concat_pad, first_layer_pad=112)
    conv_vgg(n, n.fg_image, suffix='m', last_layer_pad=concat_pad, first_layer_pad=112)
    conv_vgg(n, n.next_image, suffix='n', last_layer_pad=0, first_layer_pad=112)
    
    # concatination
    n.concat1 = L.Concat(n.relu5_3c, n.relu5_3m, n.relu5_3n)
    
    # fully conv
    if initialize_fcs:
        std = .01
    else:
        std = -1
    n.fc6, n.relu6 = conv_relu(n.concat1, fc6_size, ks=7, dilation=4, std=std)
    if split == 'train':
        n.drop6 = L.Dropout(n.relu6, dropout_ratio=0.5, in_place=True)
        n.fc7, n.relu7 = conv_relu(n.drop6, fc7_size, ks=1, pad=0, std=std)
        n.drop7 = L.Dropout(n.relu7, dropout_ratio=0.5, in_place=True)
        n.fc8 = L.Convolution(n.drop7, kernel_size=1, param=[dict(lr_mult=1, decay_mult=1), dict(lr_mult=2, decay_mult=0)], num_output=2)
    else:
        n.fc7, n.relu7 = conv_relu(n.relu6, fc7_size, ks=1, pad=0, std=std)
        if initialize_fcs:
            n.fc8 = L.Convolution(n.relu7, kernel_size=1, param=[dict(lr_mult=1, decay_mult=1), dict(lr_mult=2, decay_mult=0)], weight_filler=dict(type='gaussian', std=std), num_output=2)
        else:
            n.fc8 = L.Convolution(n.relu7, kernel_size=1, param=[dict(lr_mult=1, decay_mult=1), dict(lr_mult=2, decay_mult=0)], num_output=2)
        
    
    n.upscore = L.Deconvolution(n.fc8, convolution_param=dict(kernel_size=16, stride=8, num_output=2, group=2, weight_filler=dict(type='bilinear'),
                                                              bias_term=False), param=dict(lr_mult=0, decay_mult=0))

    n.score = L.Crop(n.upscore, n.next_image,
                  crop_param=dict(axis=ax + 1,  # +1 for first cropping dim.
                                  offset=list(-np.round(b).astype(int))))


def deepmask_net(n, split, cur_shape = None, next_shape = None, fc6_size = 512, fc7_size = 512, fc6_kernel = 3, upscore_scale = 4, initialize_fcs = False):
    #Create network
    if cur_shape is None or next_shape is None:
        concat_pad = np.zeros((2,))
    else:
      concat_pad = (next_shape - cur_shape)/2.0/16.0
    if not all(concat_pad == np.round(concat_pad)):
        print concat_pad
        raise Exception

    
    conv_vgg(n, n.current_image, suffix='c', last_layer_pad=concat_pad, use_dilated_net = False)
    conv_vgg(n, n.fg_image, suffix='m', last_layer_pad=concat_pad, use_dilated_net = False)
    conv_vgg(n, n.next_image, suffix='n', last_layer_pad=0, use_dilated_net = False)
    
    # concatination
    n.concat1 = L.Concat(n.relu5_3c, n.relu5_3m, n.relu5_3n)
    
    inner_product_param = dict()
    if initialize_fcs:
        std = .01
        inner_product_param['weight_filler'] = dict(type='gaussian', std=std)
    else:
        std = -1
    n.fc6, n.relu6 = conv_relu(n.concat1, fc6_size, pad = (fc6_kernel - 1)/2, ks=fc6_kernel, std=std)
    n.fc7 = L.InnerProduct(n.fc6, num_output=fc7_size, **inner_product_param)
    n.fc8 = L.InnerProduct(n.fc7, num_output=2*next_shape[0]*next_shape[1]/upscore_scale/upscore_scale, **inner_product_param)
    
    
    #reshape Nx(2xscore_hxscore_w) to Nx2xscore_wxscore_h
    n.reshape = L.Reshape(n.fc8, reshape_param=dict(shape=dict(dim=[0, 2, next_shape[0]/upscore_scale, next_shape[1]/upscore_scale])))
    
    n.score = L.Deconvolution(n.reshape, convolution_param=dict(kernel_size=2 * upscore_scale - upscore_scale % 2, stride=upscore_scale, pad=int(np.ceil((upscore_scale - 1)/2.0)), num_output=2, group=2, weight_filler=dict(type='bilinear'),
                                                              bias_term=False), param=dict(lr_mult=0, decay_mult=0))


def create_datalayer(n, split, datalayer_info):
        #Create datalayer
    if split != 'deploy':
        if datalayer_info['name'] == 'coco' or datalayer_info['name'] == 'pascal':
            if split == 'train':
                coco_split = 'training'
            elif split == 'val':
                coco_split = 'test'
        
            if not datalayer_info.has_key('inverse_flow'):
                datalayer_info['flow_scale'] = 1.0
                datalayer_info['inverse_flow'] = False
                
            pydata_params = dict(dataset = datalayer_info['name'], 
                                batch_size=datalayer_info['batch_size'], 
                                cur_shape=tuple(datalayer_info['cur_shape']), 
                                next_shape=tuple(datalayer_info['next_shape']), 
                                num_threads=datalayer_info['num_threads'], 
                                max_queue_size=datalayer_info['max_queue_size'], split=coco_split, mean=tuple(datalayer_info['mean']),
                                data_dir=datalayer_info['data_dir'],
                                noisy_mask=datalayer_info['noisy_mask'],
                                bgr=datalayer_info['bgr'],
                                scale_256=datalayer_info['scale_256'],
                                bb1_enlargment=datalayer_info['bb1_enlargment'],
                                bb2_enlargment=datalayer_info['bb2_enlargment'],
                                inverse_flow=datalayer_info['inverse_flow'],
                                flow_scale=datalayer_info['flow_scale'])
            module = 'coco_transformed_datalayers_prefetch'
            layer = 'CocoTransformedDataLayerPrefetch'

            if datalayer_info['inverse_flow']:
                    n.current_image, n.fg_image, n.next_image, n.label, n.iflow = L.Python(module=module, layer=layer, ntop=5, param_str=str(pydata_params))
            else:
                n.current_image, n.fg_image, n.next_image, n.label = L.Python(module=module, layer=layer, ntop=4, param_str=str(pydata_params))
        elif datalayer_info['name'] == 'davis':
            if split == 'train':
                davis_split = 'training' #Since Amir Is a Stupid Person!
            elif split == 'val':
                davis_split = 'test'
                
            pydata_params = dict(batch_size=datalayer_info['batch_size'], 
                                im_shape=tuple(datalayer_info['next_shape']), 
                                num_threads=datalayer_info['num_threads'], 
                                max_queue_size=datalayer_info['max_queue_size'], split=davis_split, mean=tuple(datalayer_info['mean']))
            module = 'davis_datalayer_prefetch_randomer'
            layer = 'DavisDataLayerPrefetch'
            
            n.current_image, n.fg_image, n.next_image, n.label = L.Python(module=module, layer=layer, ntop=4, param_str=str(pydata_params))
            
        elif datalayer_info['name'] == 'davis_sequence':
            if split == 'train':
                davis_split = 'training'
            elif split == 'val':
                davis_split = 'test'
            pydata_params = dict(batch_size=datalayer_info['batch_size'], 
                                cur_shape=tuple(datalayer_info['cur_shape']), 
                                next_shape=tuple(datalayer_info['next_shape']), 
                                split=davis_split,
                                port = datalayer_info['port'],
                                mean=tuple(datalayer_info['mean']),
                                bgr=datalayer_info['bgr'],
                                scale_256=datalayer_info['scale_256'],
                                bb1_enlargment=datalayer_info['bb1_enlargment'],
                                bb2_enlargment=datalayer_info['bb2_enlargment'],
                                augmentations=datalayer_info['augmentations'],
                                mask_mean=datalayer_info['mask_mean'],
                                flow_method =datalayer_info['flow_method'],
                                mask_threshold=datalayer_info['mask_threshold'],
                                extra_steps=datalayer_info['extra_steps'])
            if datalayer_info.has_key('shuffle'):
                pydata_params['shuffle'] = datalayer_info['shuffle']
            if datalayer_info.has_key('max_len'):
                pydata_params['max_len'] = datalayer_info['max_len']
            module = 'davis_datalayer_server'
            layer = 'DavisDataLayerServer'
            
            if not datalayer_info.has_key('flow_params'):
                datalayer_info['flow_params'] = []
            
            top_num = len(datalayer_info['top_names'])
            tops = L.Python(module=module, layer=layer, ntop=top_num + len(datalayer_info['flow_params']), param_str=str(pydata_params))
            
            for i in range(top_num):
                setattr(n, datalayer_info['top_names'][i], tops[i])
            for i in range(len(datalayer_info['flow_params'])):
                setattr(n, datalayer_info['flow_params'][i][0], tops[top_num + i])
        else:
            raise Exception
    elif split == 'deploy':
         n.current_image, n.label_1 = L.MemoryData(batch_size=datalayer_info['batch_size'], channels=3, height=datalayer_info['cur_shape'][0], width=datalayer_info['cur_shape'][1], ntop=2)
         n.fg_image, n.label_2 = L.MemoryData(batch_size=datalayer_info['batch_size'], channels=3, height=datalayer_info['cur_shape'][0], width=datalayer_info['cur_shape'][1], ntop=2)
         n.bg_image, n.label_3 = L.MemoryData(batch_size=datalayer_info['batch_size'], channels=3, height=datalayer_info['cur_shape'][0], width=datalayer_info['cur_shape'][1], ntop=2)
         n.next_image, n.label_4 = L.MemoryData(batch_size=datalayer_info['batch_size'], channels=3, height=datalayer_info['next_shape'][0], width=datalayer_info['next_shape'][1], ntop=2)
         n.current_mask, n.label_5 = L.MemoryData(batch_size=datalayer_info['batch_size'], channels=1, height=datalayer_info['cur_shape'][0], width=datalayer_info['cur_shape'][1], ntop=2)
         n.silence_label_1 = L.Silence(n.label_1, ntop=0)
         n.silence_label_2 = L.Silence(n.label_2, ntop=0)
         n.silence_label_3 = L.Silence(n.label_3, ntop=0)
         n.silence_label_4 = L.Silence(n.label_4, ntop=0)
         n.silence_label_5 = L.Silence(n.label_5, ntop=0)
  
         for i in range(len(datalayer_info['flow_params'])):
            data, label = L.MemoryData(batch_size=datalayer_info['batch_size'], channels=2, height=-1, width=-1, ntop=2)
            setattr(n, datalayer_info['flow_params'][i][0], data)
    else:
        raise Exception
    
def create_net(split, net_info, datalayer_info):
    n = caffe.NetSpec()
    create_datalayer(n, split, datalayer_info)

    #Create net
    if net_info['name'] == 'simple':
        simple_net(n, split, fc6_size = net_info['fc6_size'], fc7_size = net_info['fc7_size'], 
            initialize_fcs=net_info['initialize_fcs'], cur_shape = datalayer_info['cur_shape'], 
            next_shape = datalayer_info['next_shape'])    
    elif net_info['name'] == 'deepmask':
        deepmask_net(n, split, cur_shape = datalayer_info['cur_shape'], next_shape = datalayer_info['next_shape'], 
              fc6_size = net_info['fc6_size'], fc7_size = net_info['fc7_size'], initialize_fcs=net_info['initialize_fcs'], 
              fc6_kernel = net_info['fc6_kernel'], upscore_scale = net_info['upscore_scale'])
    elif net_info['name'] == 'fcn8':
        tmp_net = caffe.NetSpec()
        tmp_net.next_image, tmp_net.label = L.MemoryData(batch_size=1, channels=3, height=244, width=244, ntop=2)
        crop_params = fcn8_net(tmp_net, split, cur_shape = datalayer_info['cur_shape'], next_shape = datalayer_info['next_shape'], 
                fc6_size = net_info['fc6_size'], fc7_size = net_info['fc7_size'], crop_params = None)
        fcn8_net(n, split, cur_shape = datalayer_info['cur_shape'], next_shape = datalayer_info['next_shape'], 
                fc6_size = net_info['fc6_size'], fc7_size = net_info['fc7_size'], crop_params = crop_params, initialize_fcs=net_info['initialize_fcs'])
    elif net_info['name'].startswith('match'):
        match_net(n, split, cur_shape = datalayer_info['cur_shape'], next_shape = datalayer_info['next_shape'], 
              fc6_size = net_info['fc6_size'], fc7_size = net_info['fc7_size'], nrefinemnet_unit=net_info['nrefinemnet_unit'], 
              filler_param=net_info['filler_param'], share_weights = net_info['share_weights'], 
              visible_flow = net_info['visible_flow'], flow_scale=net_info['flow_scale'], compute_obj_flow=net_info['unsup_flow_loss'])
    elif net_info['name'].startswith('epicmatch'):
        disp_params = epicmatch_net(n, split, cur_shape = datalayer_info['cur_shape'], next_shape = datalayer_info['next_shape'], 
              fc6_size = net_info['fc6_size'], fc7_size = net_info['fc7_size'], nrefinemnet_unit=net_info['nrefinemnet_unit'], 
              filler_param=net_info['filler_param'], share_weights = net_info['share_weights'], 
              flow_params = net_info['flow_params'], bg_method = net_info['bg_method'])
        
        #update param_str of datalayer
        datalayer_flow_param = []
        for disp_param in disp_params:
            if disp_param is not None:
                datalayer_flow_param.append(disp_param)
        
        if split != 'deploy' and n.current_image.fn.params.has_key('param_str'):
            datalayer_param = eval(n.current_image.fn.params['param_str'])
            datalayer_param['flow_params'] = datalayer_flow_param
            n.current_image.fn.params['param_str'] = str(datalayer_param)
        elif split == 'deploy':
            for i in range(len(datalayer_flow_param)):
                name, down_scale, offset, flow_scale = datalayer_flow_param[i]
                data = getattr(n, name)
                pad = -offset + (down_scale - 1)/2
                assert pad == int(pad) and pad >= 0 and offset <= 0
                data.fn.params['height'] = int(np.floor(float(datalayer_info['next_shape'][0] + 2 * pad) / down_scale))
                data.fn.params['width'] = int(np.floor(float(datalayer_info['next_shape'][1] + 2 * pad) / down_scale))
            
    mask_blob = n.score
    if net_info.has_key('enhancement_method'):
        if net_info['enhancement_method'] == 'crf':
            n.crf_score = L.MultiStageMeanfield(mask_blob, mask_blob, n.next_image, param=[dict(lr_mult=0.001, decay_mult=1), 
                                                dict(lr_mult=0.001, decay_mult=1), dict(lr_mult=0.01, decay_mult=1)], 
                                                multi_stage_meanfield_param=dict(num_iterations=10,compatibility_mode=0,threshold=2,
                                                theta_alpha=160,theta_beta=3.0/255,theta_gamma=3,spatial_filter_weight=3,
                                                bilateral_filter_weight=5))
            mask_blob = n.crf_score
        
    if split != 'deploy':
        #if net_info.has_key('visible_flow') and net_info.has_key('inverse_flow') and net_info['visible_flow'] and datalayer_info['inverse_flow']:
            #disp_weight = 1.0
            #if net_info['nrefinemnet_unit'] > 1:
                #disp_weight = 0.5
                #n.disp_m4_loss = L.L1Loss(n.visible_disp_m4, n.iflow, l1_loss_param=dict(l2_per_location=True, normalize_by_num_entries=True), loss_weight=disp_weight)
            #n.disp_m5_loss = L.L1Loss(n.visible_disp_m5, n.iflow, l1_loss_param=dict(l2_per_location=True, normalize_by_num_entries=True), loss_weight=disp_weight)
        if net_info.has_key('unsup_flow_loss') and net_info['unsup_flow_loss']:
            n.warped_obj = L.FlowWarping(n.cur_masked_gt, n.obj_flow, flow_warping_param = dict(scale = 1/net_info['flow_scale']))
            n.unsup_flow_loss = L.EuclideanLoss(n.warped_obj, n.next_masked_gt, loss_weight=1e-2)
            
        n.loss = L.SoftmaxWithLoss(mask_blob, n.label,
                                   loss_param=dict(ignore_label=255), loss_weight=1.0)
        
            
    else:
        n.prop = L.Softmax(mask_blob)
    
    if datalayer_info['name'] == 'davis_sequence':
        pydata_params = dict(port=datalayer_info['port'])
        module = 'davis_datalayer_client'
        layer = 'DavisDataLayerClient'
        n.clinet_top = L.Python(mask_blob, module=module, layer=layer, ntop=1, param_str=str(pydata_params))
    return n
