import caffe

import numpy as np
import os
from matplotlib.pyplot import imshow, show, figure
from skimage import io



def show(net, mean = [0, 0, 0], forward = False, score_threshold = 0, out = './result/', prefix = ''):
    if forward:
      net.forward()
      
    mean = mean.reshape((1, 1, 1, 3))
    cur_im = net.blobs['cur_im'].data.transpose((0, 2, 3, 1))[:, :, :, ::-1] + mean
    masked_im = net.blobs['masked_im'].data.transpose((0, 2, 3, 1))[:, :, :, ::-1] + mean
    next_im = net.blobs['next_im'].data.transpose((0, 2, 3, 1))[:, :, :, ::-1] + mean
    label = net.blobs['label'].data
    #f32 = net.blobs['f32'].data.transpose((0,2,3,1))[:, :, :, ::-1]
    score = np.exp(net.blobs['score'].data)
    score = score[:,1]/score.sum(1)
    
    if score_threshold > 0:
      score = score > score_threshold
    batch_size = cur_im.shape[0]
    
    if prefix != '':
	prefix = prefix + '_'
    for i in xrange(batch_size):
	name = out + '%s%d_cur_im.png' % (prefix, i)
	io.imsave(name, (cur_im[i]).astype('uint8'))
	name = out + '%s%d_masked_im.png' % (prefix, i)
	io.imsave(name, (masked_im[i]).astype('uint8'))
	name = out + '%s%d_next_im.png' % (prefix, i)
	io.imsave(name, (next_im[i]).astype('uint8'))
	name = out + '%s%d_label.png' % (prefix, i)
	io.imsave(name, (label[i][0]*255).astype('uint8'))
	name = out + '%s%d_score.png' % (prefix, i)
	io.imsave(name, (score[i]*255).astype('uint8'))
    






model = '../val_davis_sequence_epicmatch2.prototxt'
weights = '../snapshots/davis_sequence_epicmatch2_noflow_iter_51836.caffemodel'
# init
caffe.set_device(0)
caffe.set_mode_gpu()

net = caffe.Net(model, weights, caffe.TRAIN)

mean = np.array((0.40787054, 0.45752458, 0.48109378))[::-1] * 255
for i in range(1327):
    print 'Iteration', i
    show(net, mean, True, 0, prefix = str(i))
