import numpy as np
from skimage.transform import resize
import os.path as osp
from scipy import misc
import random
import math
from skimage.morphology import disk
from skimage.filters import rank
import pickle
import copy
import PIL.Image as Image
debug_mode = False
def cprint(string, style = None):
    if not debug_mode and style != bcolors.FAIL and style != bcolors.OKBLUE:
        return
    if style is None:
        print str(string)
    else:
        print style + str(string) + bcolors.ENDC

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def read_img(img_path):
    cprint('Reading Image ' + img_path, bcolors.OKGREEN)
    uint_image = np.array(Image.open(img_path))
    if len(uint_image.shape) == 2:
        tmp_image = np.zeros(uint_image.shape + (3,), dtype=np.uint8)
        tmp_image[:,:,0] = tmp_image[:,:,1] = tmp_image[:,:,2] = uint_image
        uint_image = tmp_image
    return np.array(uint_image, dtype=np.float32)/255.0
        
def read_mask(mask_path):
    #read mask
    m_uint = np.array(Image.open(mask_path))
    fg = np.unique(m_uint)
    if not (len(m_uint.shape) == 2 and ((len(fg) == 2 and fg[0] == 0 and fg[1] == 255) or (len(fg) == 1 and (fg[0] == 0 or fg[0] == 255)))):
        print mask_path, fg, m_uint.shape
        raise Exception
    return np.array(m_uint, dtype=np.float32) / 255.0
    
    
def read_flo_file(file_path):
    """
    reads a flo file, it is for little endian architectures,
    first slice, i.e. data2D[:,:,0], is horizontal displacements
    second slice, i.e. data2D[:,:,1], is vertical displacements

    """
    with open(file_path, 'rb') as f:
        magic = np.fromfile(f, np.float32, count=1)
        if 202021.25 != magic[0]:
            cprint('Magic number incorrect. Invalid .flo file: %s' % file_path, bcolors.FAIL)
            raise  Exception('Magic incorrect: %s !' % file_path)
        else:
            w = np.fromfile(f, np.int32, count=1)
            h = np.fromfile(f, np.int32, count=1)
            data = np.fromfile(f, np.float32, count=2*w*h)
            data2D = np.reshape(data, (h[0], w[0], 2), order='C')
            return data2D


def write_flo_file(file_path, data2D):
    """
    writes a flo file, it is for little endian architectures,
    first slice, i.e. data2D[:,:,0], is horizontal displacements
    second slice, i.e. data2D[:,:,1], is vertical displacements

    """
    with open(file_path, 'wb') as f:
        magic = np.array(202021.25, dtype='float32')
        magic.tofile(f)
        h = np.array(data2D.shape[0], dtype='int32')
        w = np.array(data2D.shape[1], dtype='int32')
        w.tofile(f)
        h.tofile(f)
        data2D.astype('float32').tofile(f);
        
def add_noise_to_mask(cmask, r_param = (15, 15), mult_param = (20, 5), threshold = .2):
    radius = max(np.random.normal(*r_param), 1)
    mult = max(np.random.normal(*mult_param), 2)
    
    selem = disk(radius)
    mask2d = np.zeros(cmask.shape + (2,))
    mask2d[:, :, 0] = rank.mean((1 - cmask).copy(), selem=selem) / 255.0
    mask2d[:, :, 1] = rank.mean(cmask.copy(), selem=selem) / 255.0

    exp_fmask = np.exp(mult * mask2d);
    max_fmask = exp_fmask[:,:,1] / np.sum(exp_fmask, 2);
    max_fmask[max_fmask < threshold] = 0;
    
    return max_fmask
    
def bbox(img):
    if img.sum() == 0:
        return -1, -1, -1, -1
    rows = np.any(img, axis=1)
    cols = np.any(img, axis=0)
    rmin, rmax = np.where(rows)[0][[0, -1]]
    cmin, cmax = np.where(cols)[0][[0, -1]]
    return rmin, rmax, cmin, cmax

def crop(img, bbox, bbox_enargement_factor = 1, constant_pad = 0, output_shape = None, resize_order = 1, clip = True):
    pad_size = ((bbox_enargement_factor - 1) * np.array([bbox[1]+1 - bbox[0], bbox[3]+1 - bbox[2]], 'float32')/2.0 + constant_pad).astype('int')
    pad_param = tuple(zip(pad_size, pad_size)) + (len(img.shape) - 2) * ((0,0),)
    img_padded = np.pad(img, pad_param, mode='constant')
    output = img_padded[bbox[0]:(bbox[1] + 2 * pad_size[0] + 1), bbox[2]:(bbox[3] + 2 * pad_size[1] + 1)]
    if output_shape is not None:
        return resize( output, output_shape, order = resize_order, mode = 'nearest', clip = clip)
    else:
        return output

def crop_undo(img, bbox, bbox_enargement_factor = 1, constant_pad = 0, output_shape = None, resize_order = 1):
    #if len(img.shape) != 2:
    #    raise Exception
    pad_size = ((bbox_enargement_factor - 1) * np.array([bbox[1]+1 - bbox[0], bbox[3]+1 - bbox[2]], 'float32')/2.0 + constant_pad).astype('int')
    correct_size = np.array([bbox[1] - bbox[0] + 1, bbox[3] - bbox[2] + 1]) + 2 * pad_size
    img = resize(img, correct_size, order = resize_order, mode = 'nearest')
    if output_shape is not None:
        output = np.zeros((output_shape[0] + 2 * pad_size[0], output_shape[1] + 2 * pad_size[1]) + img.shape[2:])    
        output[bbox[0]:(bbox[1] + 2 * pad_size[0] + 1), bbox[2]:(bbox[3] + 2 * pad_size[1] + 1)] = img
        #print output.shape, img.shape, (output_shape[0] + 2 * pad_size[0], output_shape[1] + 2 * pad_size[1]) + img.shape[2:]
        #if len(img.shape) > 2:
        #    raise Exception
        return output[pad_size[0]:-pad_size[0], pad_size[1]:-pad_size[1]]
    else:
        return img
    
#defaults is a list of (key, val) is val is None key is required field
def check_params(params, **kwargs):
    for key, val in kwargs.items():
        key_defined = (key in params.keys())
        if val is None:
            assert key_defined, 'Params must include {}'.format(key)
        elif not key_defined:
            params[key] = val

def load_netflow_db(annotations_file, split, shuffle = False):
    if split == 'training':
        split = 1
    if split == 'test':
        split = 2
    annotations = np.loadtxt(annotations_file)
    frame_indices = np.arange(len(annotations))
    frame_indices = frame_indices[ annotations == split ]
    length = len(frame_indices)
    data_dir = osp.join(osp.dirname(osp.abspath(annotations_file)), 'data/')
    if shuffle:
        random.shuffle( frame_indices)

    return dict(frame_indices=frame_indices, data_dir=data_dir, length=length)

def read_netflow_instance(netflow_db, instance_id):
    data_dir = netflow_db['data_dir']
    instance_id = netflow_db['frame_indices'][instance_id]
    instance_id = instance_id + 1
    img1 = np.array(Image.open(osp.join(data_dir, '%05d_img1.ppm' % instance_id)).astype(np.float32)) / 255.0
    img2 = np.array(Image.open(osp.join(data_dir, '%05d_img2.ppm' % instance_id)).astype(np.float32)) / 255.0
    flow = read_flo_file( osp.join(data_dir, '%05d_flow.flo' % instance_id))
    return img1, img2, flow


#def compute_flow(T1, T2, object_size, img_size, flow = None):
        #newx = np.arange(img_size[1])
        #newy = np.arange(img_size[0])
        #mesh_grid = np.stack(np.meshgrid(newx, newy), axis = 0)
        #locs1 = np.array(mesh_grid, dtype='float')
        #locs2 = np.array(locs1.copy(), dtype='float')
        #if flow is not None:
             #locs2 += flow.transpose((2,0,1))
        #x,y = T1.transform_points(locs1[0].ravel(), locs1[1].ravel(), locs1[0].shape)
        #locs1 = np.concatenate((x,y)).reshape((2,) + img_size)
        #x,y = T2.transform_points(locs2[0].ravel(), locs2[1].ravel(), locs2[0].shape)
        #locs2 = np.concatenate((x,y)).reshape((2,) + img_size)
        #flow_trans = locs2 - locs1
        
        #final_flow = np.zeros((2,) + img_size)
        #T1_cp = copy.deepcopy(T1)
        #T1_cp.color_adjustment_param = None
        #final_flow[0] = T1_cp.transform_img(flow_trans[0], object_size, flow_trans[0].shape, cval=0)
        #final_flow[1] = T1_cp.transform_img(flow_trans[1], object_size, flow_trans[1].shape, cval=0)
        #return final_flow.transpose((1,2,0))


def compute_flow(T1, T2, object_size, img_size, flow = None):
    assert len(img_size) == 2 and len(object_size) == 2
    #final_flow(T1(i,j)) = T2( (i,j) + f1(i,j) ) - T1(i,j)
    # = (T2(i,j) + T2(f1(i,j)) - T2((0,0))) - T1(i, j)
    # = T2(i,j) - T1(i,j) + T2(f1(i,j)) - T2((0,0))
    # = A + B - T2((0,0)) where A = T2(i,j) - T1(i,j), B = T2(f1(i,j))
    # A(T1(i, j)) = T2(i,j) - T1(i,j) ==> A((m,n)) = T2(T1^-1(m,n)) - (m, n)
    # B(T1(i, j)) = T2(f1(i,j)) ==(see *)==> BT1(m,n) = B(T^-1(m,n))
        
    # * Given an image I and transformation T: (IT is the image after applying transformation T)
    # IT[k,l] = I[T^-1(k,l)]
        
    # 1) Compute A
    newx = np.arange(img_size[1])
    newy = np.arange(img_size[0])
    mesh_grid = np.stack(np.meshgrid(newx, newy), axis = 0)
    locs1 = np.array(mesh_grid, dtype='float')
    x,y = T1.itransform_points(locs1[0].ravel(), locs1[1].ravel(), object_size)
    x,y = T2.transform_points(x, y, object_size)
    locs2 = np.concatenate((x,y)).reshape((2,) + locs1[0].shape)
    final_flow = locs2 - locs1
        
    # 2) Compute B - T2((0,0))
    if flow is not None:
        # B
        x,y = T2.transform_points(flow[:,:,0].ravel(), flow[:,:,1].ravel(), object_size)
        b_flow = np.concatenate((x,y)).reshape((2,) + img_size)
        T1_cp = copy.deepcopy(T1)
        T1_cp.color_adjustment_param = None
        b_flow[0] = T1_cp.transform_img(b_flow[0], object_size, b_flow[0].shape, cval=0)
        b_flow[1] = T1_cp.transform_img(b_flow[1], object_size, b_flow[1].shape, cval=0)
        #T2((0,0))
        x0, y0 = T2.transform_points(np.array((0,)), np.array((0,)), object_size)
        b_flow[0] -= x0[0]
        b_flow[1] -= y0[0]
        
        #Add it to the final flow
        final_flow += b_flow
        
    
    return final_flow.transpose((1,2,0))
        
def sample_trans(base_tran, trans_dist):
    if base_tran is None and trans_dist is None:
        return None
    if base_tran is None:
        return trans_dist.sample()
    if trans_dist is None:
        return base_tran
    return base_tran + trans_dist.sample()
    
########################################################################### Sequence Generator ########################################################################

class VideoPlayer:
    def __init__(self, video_item, base_trans, frame_trans_dist, step = 1, offset = 0, max_len = np.inf, flo_method = None):
        self.name = video_item.name
        if offset != 0:
            self.name += '_o' + str(offset)
            
        if step != 1:
            self.name += '_s' + str(step)
            
        if not np.isinf(max_len):
            self.name += '_m' + str(max_len)
        
        self.video_item = video_item
        self.step = step
        self.offset = offset
        self.max_len = max_len
        self.flo_method = flo_method
        
        ##compute img_ids
        if step > 0:
            a = self.offset
            b = self.video_item.length
        elif step < 0:
            a = self.video_item.length - 1 - self.offset
            b = - 1    
        self.img_ids = range(a, b, self.step)
        if not np.isinf(self.max_len):
            self.img_ids = self.img_ids[:self.max_len]
        self.length = len(self.img_ids)
        #cprint(str(self.img_ids), bcolors.OKBLUE)
        ##compute mappings
        self.mappings = None
        if base_trans is not None or frame_trans_dist is not None:
            self.name += '_' + str(random.randint(0, 1e10))
            self.mappings = []
            for i in range(self.length):
                mapping = sample_trans(base_trans, frame_trans_dist)
                self.mappings.append(mapping)
            
    def get_frame(self, frame_id, compute_iflow = False):
        img_id = self.img_ids[frame_id]
        img = self.video_item.read_img(img_id)
        output = dict(image=img)
        mask = None
        try:
            mask = self.video_item.read_mask(img_id)
            if self.mappings is not None:
                obj_bbox = bbox(mask)
                obj_size = (obj_bbox[1] - obj_bbox[0], obj_bbox[3] - obj_bbox[2])
                img = self.mappings[frame_id].transform_img(img.copy(), obj_size, img.shape[:2], mask)
                mask  = self.mappings[frame_id].transform_mask(mask.copy(), obj_size, mask.shape)[0]
                mask[mask == -1] = 0
        except IOError:
            cprint('Failed to load mask \'' + str(img_id) + '\' for video \'' + self.name + '\'. Return None mask..', bcolors.FAIL)
        
        output = dict(image=img, mask=mask)
        
        if compute_iflow:
            try:
                iflow = self.video_item.read_iflow(img_id, self.step, self.flo_method)
            except Exception as e:
                cprint('Failed to load \'' + self.flo_method + '\' iflow for video ' + self.name + '. Return zero iflow..', bcolors.FAIL)
                iflow = np.zeros(img.shape[:2] + (2,))
            if self.mappings is None:
                output['iflow'] = iflow
            else:
                output['iflow'] = compute_flow(self.mappings[frame_id], self.mappings[frame_id - 1], obj_size, img.shape[:2], flow = iflow)
                
        return output    

class ImagePlayer:
    def __init__(self, image_item, base_trans, frame_trans_dist, compute_iflow = False, length = 2):
        self.name = image_item.name + '_' + str(random.randint(0, 1e10))
        self.length = length
        self.imgs = []
        self.masks = []
        
        img = image_item.read_img()
        mask = image_item.read_mask()
        obj_bbox = bbox(mask)
        obj_size = (obj_bbox[1] - obj_bbox[0], obj_bbox[3] - obj_bbox[2])
        mappings = []
        for i in range(length):
            mapping = sample_trans(base_trans, frame_trans_dist)
            assert mapping is not None
            timg = mapping.transform_img(img.copy(), obj_size, img.shape[:2], mask)
            tmask  = mapping.transform_mask(mask.copy(), obj_size, mask.shape)[0]
            tmask[tmask == -1] = 0          
            self.imgs.append(timg)
            self.masks.append(tmask)
            mappings.append(mapping)
            
        if compute_iflow:
            self.iflows = [None]
            for i in range(1, length):
                iflow = compute_flow(mappings[i], mappings[i - 1], obj_size, mask.shape)
                self.iflows.append(iflow)

    def get_frame(self, frame_id, compute_iflow = False):
        output = dict(image=self.imgs[frame_id], mask=self.masks[frame_id])
        if compute_iflow:
            output['iflow'] = self.iflows[frame_id]
        return output 
 
########################################################################### Read DBs into DBItems ################################################################################
class DAVIS:
    davis = __import__('davis')
    def __init__(self):
        pass

    # DAVIS: 1376 Test, 2079 Training
    # Jump-Cut: ?
    def getItems(self, sets):
        if isinstance(sets, basestring):
            sets = [sets]
        db_info = self.davis.dataset.utils.db_read_info()
        sequences = [x for x in db_info.sequences if x['set'] in sets]
        assert len(sequences) > 0
        
        items = []
        for seq in sequences:
            name = seq['name']
            img_root = osp.join(self.davis.cfg.PATH.SEQUENCES_DIR, name)
            ann_root = osp.join(self.davis.cfg.PATH.ANNOTATION_DIR, name)
            item = DBDAVISItem(name, img_root, ann_root, seq['num_frames'])
            items.append(item)
        return items

class COCO:
    pycocotools = __import__('pycocotools.coco')
    def __init__(self, db_path, dataType):
        if dataType == 'training':
            dataType = 'train2014'
        elif dataType == 'test':
            dataType = 'val2014'
        else:
            raise Exception('split \'' + dataType + '\' is not valid! Valid splits: training/test')
        
        self.db_path = db_path
        self.dataType = dataType
        
    def getItems(self, cats=[], areaRng=[], iscrowd=False):
        
        annFile='%s/annotations/instances_%s.json' % (self.db_path, self.dataType)
        
        coco = self.pycocotools.coco.COCO(annFile)
        catIds = coco.getCatIds(catNms=cats);
        anns = coco.getAnnIds(catIds=catIds, areaRng=areaRng, iscrowd=iscrowd)
        cprint(str(len(anns)) + ' annotations read from coco', bcolors.OKGREEN)
        
        items = []
        for i in range(len(anns)):
            ann = anns[i]
            item = DBCOCOItem('coco-'  + self.dataType + str(i), self.db_path, self.dataType, ann, coco, self.pycocotools)
            items.append(item)
        return items    
        
        
class PASCAL:
    def __init__(self, db_path, dataType):
        if dataType == 'training':
            dataType = 'train'
        elif dataType == 'test':
            dataType = 'val'
        else:
            raise Exception('split \'' + dataType + '\' is not valid! Valid splits: training/test')
        
        self.db_path = db_path
        classes = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car' , 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse', 'motorbike', 'person', 'potted plant', 'sheep', 'sofa', 'train', 'tv/monitor']
        self.name_id_map = dict(zip(classes, range(1, len(classes) + 1)))
        self.dataType = dataType
     
    def getCatIds(self, catNms=[]):
        return [self.name_id_map[catNm] for catNm in catNms]
    
    def get_annotation_path(self):
        return osp.join(self.db_path, self.dataType + '_anns.pkl')
    
    def load_annotations(self):
        path = self.get_annotation_path()
        if not osp.exists(path):
            self.create_annotations()
        
        with open(path, 'rb') as f:
            anns = pickle.load(f)
        return anns
    
    def create_annotations(self):
        with open(osp.join(self.db_path, 'ImageSets', 'Segmentation', self.dataType + '.txt'), 'r') as f:
            names = f.readlines()    
            names = [name[:-1] for name in names]
        anns = []
        for item in names:
            mobj_path = osp.join(self.db_path, 'SegmentationObject', item + '.png')
            mobj_uint = misc.imread(mobj_path)
            obj_ids, obj_sizes = np.unique(mobj_uint, return_counts=True)
            mclass_path = osp.join(self.db_path, 'SegmentationClass', item + '.png')
            mclass_uint = misc.imread(mclass_path)
            
            for obj_idx in xrange(len(obj_ids)):
                class_id = int(np.median(mclass_uint[mobj_uint == obj_ids[obj_idx]]))
                if class_id == 0 or class_id == 255 or obj_ids[obj_idx] == 0 or obj_ids[obj_idx] == 255:
                    continue
                anns.append(dict(image_name=item, mask_name=item, object_id=obj_ids[obj_idx], class_id=class_id, object_size = obj_sizes[obj_idx]))
        
        with open(self.get_annotation_path(), 'w') as f:
            pickle.dump(anns, f)
    
    #def readAnn(ann):
    #image_path = 
    #mask_path = 
    #obect_id = 
    #return (image_path, mask_path, object_id
    #Each annotion has image_name, mask_name, object_id, class_id, object_size keys
    def getAnns(self, catIds=[], areaRng=[]):
        if areaRng == []:
            areaRng = [0, np.inf]
        anns = self.load_annotations()
        if catIds == []:
            if areaRng == [0, np.inf]:
                return anns
            catIds = self.getCatIds(self.nam_id_map.keys())
        
        filtered_anns = [ann for ann in anns if ann['class_id'] in catIds and areaRng[0] < ann['object_size'] and ann['object_size'] < areaRng[1]]
        return filtered_anns
    
    def getItems(self, cats=[], areaRng=[]):
        catIds = self.getCatIds(catNms=cats)
        anns = self.getAnns(catIds=catIds, areaRng=areaRng)
        cprint(str(len(anns)) + ' annotations read from pascal', bcolors.OKGREEN)
        
        items = []
        for i in range(len(anns)):
            ann = anns[i]
            img_path = osp.join(self.db_path, 'JPEGImages', ann['image_name'] + '.jpg')
            mask_path = osp.join(self.db_path, 'SegmentationObject', ann['mask_name'] + '.png')
            item = DBPascalItem('pascal-'  + self.dataType + str(i), img_path, mask_path, ann['object_id'])
            items.append(item)
        return items
    
########################################################################### DB Items ###################################################################################
class DBDAVISItem:
    def __init__(self, name, img_root, ann_root, length):
        self.name = name
        self.length = length
        self.img_root = img_root
        self.ann_root = ann_root
    
    def read_img(self, img_id):
        file_name = osp.join(self.img_root, '%05d.jpg' % (img_id))
        return read_img(file_name)
        
    def read_mask(self, img_id):
        file_name = osp.join(self.ann_root, '%05d.png' % (img_id))
        return read_mask(file_name)
        
    def read_iflow(self, img_id, step, method):
        if method == 'LDOF':
            if step == 1:
                flow_name = osp.join(self.ann_root, '%05d_inv_LDOF.flo' % (img_id))
            elif step == -1:
                flow_name = osp.join(self.ann_root, '%05d_LDOF.flo' % (img_id))
            else:
                raise Exception('unsupported flow step for LDOF')
        elif method == 'EPIC':
            if step == 1:
                flow_name = osp.join(self.ann_root, '%05d_inv.flo' % (img_id))
            elif step == -1:
                flow_name = osp.join(self.ann_root, '%05d.flo' % (img_id))
            else:
                raise Exception('unsupported flow step for EPIC')
        else:
            raise Exception('unsupported flow algorithm')
        try:
            return read_flo_file(flow_name)
        except IOError as e:
            print "Unable to open file", str(e)#Does not exist OR no read permissions

 
    
class DBImageItem:
    def __init__(self, name):
        self.name = name
    def read_mask(self):
        pass
    def read_img(self):
        pass

class DBCOCOItem(DBImageItem):
    def __init__(self, name, db_path, dataType, ann_info, coco_db, pycocotools):
        DBImageItem.__init__(self, name)
        self.ann_info = ann_info
        self.db_path = db_path
        self.dataType = dataType
        self.coco_db = coco_db
        self.pycocotools = pycocotools
        
    def read_mask(self):
        ann = self.coco_db.loadAnns(self.ann_info)[0]
        img_cur = self.coco_db.loadImgs(ann['image_id'])[0]
        
        rle = self.pycocotools.mask.frPyObjects(ann['segmentation'], img_cur['height'], img_cur['width'])
        m_uint = self.pycocotools.mask.decode(rle)
        m = np.array(m_uint[:, :, 0], dtype=np.float32)
        return m
    
    def read_img(self):
        ann = self.coco_db.loadAnns(self.ann_info)[0]
        img_cur = self.coco_db.loadImgs(ann['image_id'])[0]
        img_path = '%s/images/%s/%s' % (self.db_path, self.dataType, img_cur['file_name'])
        return read_img(img_path)
    
    
class DBPascalItem(DBImageItem):
    def __init__(self, name, img_path, mask_path, instance_id):
        DBImageItem.__init__(self, name)
        self.img_path = img_path
        self.mask_path = mask_path
        self.instance_id = instance_id
    def read_mask(self):
        mobj_uint = np.array(Image.open(self.mask_path))
        m = np.zeros(mobj_uint.shape, dtype=np.float32)
        m[mobj_uint == self.instance_id] = 1
        fg = np.unique(m)
        return m
    def read_img(self):
        return read_img(self.img_path)
