import caffe
import numpy as np
from net import conv_relu, max_pool, save_net, create_net, conv_vgg, silentremove
from os import remove
from caffe import layers as L, params


def check_params(net):
    pass
def upsample_filt(size):
    factor = (size + 1) // 2
    if size % 2 == 1:
        center = factor - 1
    else:
        center = factor - 0.5
    og = np.ogrid[:size, :size]
    return (1 - abs(og[0] - center) / factor) * \
           (1 - abs(og[1] - center) / factor)

def interp(net, layers):
    for l in layers:
        m, k, h, w = net.params[l][0].data.shape
        print 'Initializing layer ' + l + ' parameters'
        if m != k and k != 1:
            print 'input + output channels need to be the same or |output| == 1'
            raise Exception
        if h != w:
            print 'filters need to be square'
            raise Exception
        filt = upsample_filt(h)
        net.params[l][0].data[range(m), range(k), :, :] = filt

def transplant(new_net, net, suffix=''):
    for p in net.params:
        p_new = p + suffix
        if p_new not in new_net.params:
            print 'dropping', p
            continue
        for i in range(len(net.params[p])):
            if i > (len(new_net.params[p_new]) - 1):
                print 'dropping', p, i
                break
            if net.params[p][i].data.shape != new_net.params[p_new][i].data.shape:
                print 'coercing', p, i, 'from', net.params[p][i].data.shape, 'to', new_net.params[p_new][i].data.shape
            else:
                print 'copying', p, ' -> ', p_new, i
            new_net.params[p_new][i].data.flat = net.params[p][i].data.flat

def copy_params(new_net, net, pair_param = []):
    for from_param, to_param in pair_param:
        if from_param in net.params and to_param in new_net.params:
            for i in range(len(net.params[from_param])):
                if i > (len(new_net.params[to_param]) - 1):
                    print 'dropping', from_param, i
                    break
                if net.params[from_param][i].data.shape != new_net.params[to_param][i].data.shape:
                    print 'coercing', from_param, i, 'from', net.params[from_param][i].data.shape, 'to', new_net.params[to_param][i].data.shape
                else:
                    print 'copying', from_param, ' -> ', to_param, i
                new_net.params[to_param][i].data.flat = net.params[from_param][i].data.flat

def create_dilated_net():
    n = caffe.NetSpec()
    n.im, n.label_1 = L.MemoryData(batch_size=1, channels=3, height=244, width=244, ntop=2)
    conv_vgg(n, n.im, first_layer_pad=100, add_extra_pad = True, use_dilated_net = True)
    # fully conv
    n.fc6, n.relu6 = conv_relu(n.conv5_3, 4096, ks=7, dilation=4)        
    n.fc7, n.relu7 = conv_relu(n.relu6, 4096, ks=1, pad=0)
    n.fc8 = L.Convolution(n.relu7, kernel_size=1, num_output=21)
    return n


def create_vgg_net():
    n = caffe.NetSpec()
    n.im, n.label_1 = L.MemoryData(batch_size=1, channels=3, height=244, width=244, ntop=2)
    conv_vgg(n, n.im, add_extra_pad = False, use_dilated_net = False)
    # fully connected is not necessary
    return n

def simple_net_surgery(datalayer_info, net_info):

    dilated_path = 'dilated_net_tmp.prototxt'
    dilated_weights = './data/dilation8_pascal_voc.caffemodel'
    new_weights = 'simple_net.caffemodel'
    new_path = 'new_net_tmp.prototxt'
    new_net_info = net_info.copy()
    new_net_info['initialize_fcs'] = True 
    dilated_netspec = create_dilated_net()
    new_netspec = create_net('deploy', new_net_info, datalayer_info)
    save_net(dilated_path, str(dilated_netspec.to_proto()))
    save_net(new_path, str(new_netspec.to_proto()))
    dilated_net = caffe.Net(dilated_path, dilated_weights, caffe.TRAIN)
    new_net = caffe.Net(new_path, caffe.TRAIN)
    #transplant vgg-net conv weights
    transplant(new_net, dilated_net, 'c')
    
    #transplant fc6 weights
    #new_net.params['fc6'][0].data[:, -512:][...] = dilated_net.params['fc6'][0].data[:fc6_size]
    #new_net.params['fc6'][1].data[...] = dilated_net.params['fc6'][1].data[:fc6_size]
    
    #transplant fc7 weights
    #new_net.params['fc7'][0].data[...] = dilated_net.params['fc7'][0].data[:fc7_size, :fc6_size]
    #new_net.params['fc7'][1].data[...] = dilated_net.params['fc7'][1].data[:fc7_size]

    new_net.save(new_weights)
    remove(dilated_path)
    remove(new_path)


def match_net_surgery(datalayer_info, net_info, clean):
    fcn8_path = '../data/fcn32s-heavy-pascal.prototxt'
    fcn8_weights = '../data/fcn32s-heavy-pascal.caffemodel'
    #caffe.set_mode_cpu()
    if clean:
        return
    
    #trained_match_weights = '../trained_match_net.caffemodel'
    #trained_match_weights = '/nethome/ashaban6/caffe/video_segmentation/simple_net/NN_FG/snapshots/davis_sequence_match_iter_33468.caffemodel'
    trained_match_weights = fcn8_weights
    match_net_weights = '../match_net.caffemodel'
    
    new_path = 'new_net_tmp.prototxt'
    
    newnet_info = net_info.copy()
    newnet_info['filler_param'] = dict(weight_filler=dict(type='msra'), bias_filler=dict(type='constant', value=0.))
    new_netspec = create_net('deploy', newnet_info, datalayer_info)
    save_net(new_path, str(new_netspec.to_proto()))
    if False and net_info['nrefinemnet_unit'] > 0:
        refined_match_weights = '../' + net_info['name'] + '_net.caffemodel'
        refined_net = caffe.Net(new_path, trained_match_weights, caffe.TRAIN)
        
        #skip_layers = ['pre_conv_m3', 'next_conv_m3', 'pre_conv_m4', 'next_conv_m4']
        #for skip_layer in skip_layers:
           #if refined_net.params.has_key(skip_layer):
               #print 'Dividing ', skip_layer + '\'s', 'weights by', 100
               #refined_net.params[skip_layer][0].data[...] = refined_net.params[skip_layer][0].data/100.0    
                
                
        interp_layers = [k for k in refined_net.params.keys() if 'up' in k]
        interp(refined_net, interp_layers)
        refined_net.save(refined_match_weights)
        remove(new_path)
        return
        
    
    fcn8_net = caffe.Net(fcn8_path, fcn8_weights, caffe.TRAIN)
    new_net = caffe.Net(new_path, caffe.TRAIN)
    
    #transplant vgg-net conv weights
    transplant(new_net, fcn8_net, 'm')
    transplant(new_net, fcn8_net, 'n')
    
    if net_info['fc6_size'] == 4096:
        pass

    if net_info['fc7_size'] == 4096:
        copy_params(new_net, fcn8_net, pair_param = (('fc7', 'fc7'),))
        
    interp_layers = [k for k in new_net.params.keys() if 'up' in k]
    interp(new_net, interp_layers)
    #skip_layers = ['pre_conv_m3', 'next_conv_m3', 'pre_conv_m4', 'next_conv_m4']
    #for skip_layer in skip_layers:
       #if new_net.params.has_key(skip_layer):
           ##print 'Dividing ', skip_layer + '\'s', 'weights by', 100
           #new_net.params[skip_layer][0].data[...] = new_net.params[skip_layer][0].data/100.0    

    new_net.save(match_net_weights)
    remove(new_path)
    
def epicmatch_net_surgery(datalayer_info, net_info, clean):
    fcn8_path = '../data/fcn32s-heavy-pascal.prototxt'
    fcn8_weights = '../data/fcn32s-heavy-pascal.caffemodel'
    #caffe.set_mode_cpu()
    if clean:
        return
    
    #trained_match_weights = '../trained_epicmatch_net.caffemodel'
    #trained_match_weights = '/nethome/ashaban6/caffe/video_segmentation/simple_net/NOFLOW_FG/snapshots/davis_sequence_epicmatch_iter_27861.caffemodel'
    
    match_net_weights = '../epicmatch_net.caffemodel'
    
    new_path = 'new_net_tmp.prototxt'
    
    newnet_info = net_info.copy()
    newnet_info['filler_param'] = dict(weight_filler=dict(type='msra'), bias_filler=dict(type='constant', value=0.))
    new_netspec = create_net('deploy', newnet_info, datalayer_info)
    save_net(new_path, str(new_netspec.to_proto()))
    if False and net_info['nrefinemnet_unit'] > 0:
        refined_match_weights = '../' + net_info['name'] + '_net.caffemodel'
        refined_net = caffe.Net(new_path, trained_match_weights, caffe.TRAIN)
        interp_layers = [k for k in refined_net.params.keys() if 'up' in k]
        interp(refined_net, interp_layers)
        
        ##For skip layers we divide param by 100 to get reasonable output
        #skip_layers = ['skip4n_conv_1', 'skip4m_conv_1', 'skip3n_conv_1', 'skip3m_conv_1', 'skip2n_conv_1', 'skip2m_conv_1', 'skip1n_conv_1', 'skip1m_conv_1']
        #if net_info['bg_method'] == 'FG/BG':
            #skip_layers += ['skip4b_conv_1', 'skip3b_conv_1', 'skip2b_conv_1', 'skip1b_conv_1']
        #for skip_layer in skip_layers:
            #if refined_net.params.has_key(skip_layer):
                #print 'Dividing ', skip_layer + '\'s', 'weights by', 100
                #refined_net.params[skip_layer][0].data[...] = refined_net.params[skip_layer][0].data/100.0    
        
        refined_net.save(refined_match_weights)
        #remove(new_path)
        return
        
    
    fcn8_net = caffe.Net(fcn8_path, fcn8_weights, caffe.TRAIN)
    new_net = caffe.Net(new_path, caffe.TRAIN)
    
    #transplant vgg-net conv weights
    transplant(new_net, fcn8_net, 'm')
    transplant(new_net, fcn8_net, 'n')
    if net_info['bg_method'] == 'FG/BG':
        transplant(new_net, fcn8_net, 'b')

    if net_info['fc6_size'] == 4096:
        pass

    if net_info['fc7_size'] == 4096:
        copy_params(new_net, fcn8_net, pair_param = (('fc7', 'fc7'),))
    
    #For skip layers we divide param by 100 to get reasonable output
    #skip_layers = ['skip4n_conv_1', 'skip4m_conv_1', 'skip3n_conv_1', 'skip3m_conv_1', 'skip2n_conv_1', 'skip2m_conv_1', 'skip1n_conv_1', 'skip1m_conv_1']
    #if net_info['bg_method'] == 'FG/BG':
        #skip_layers += ['skip4b_conv_1', 'skip3b_conv_1', 'skip2b_conv_1', 'skip1b_conv_1']
    #for skip_layer in skip_layers:
        #if new_net.params.has_key(skip_layer):
            #print 'Dividing ', skip_layer + '\'s', 'weights by', 100
            #new_net.params[skip_layer][0].data[...] = new_net.params[skip_layer][0].data/100.0
    
    interp_layers = [k for k in new_net.params.keys() if 'up' in k]
    interp(new_net, interp_layers)
    new_net.save(match_net_weights)
    #remove(new_path)


def fcn8_net_surgery(datalayer_info, net_info, clean):

    fcn8_path = '../data/crfasrnn_coco_voc.prototxt'
    fcn8_weights = '../data/crfasrnn_coco_voc.caffemodel'
    
    new_weights = '../fcn8_net.caffemodel'
    
    if clean:
        silentremove(new_weights)
        return
    
    new_path = 'new_net_tmp.prototxt'
    new_net_info = net_info.copy()
    new_net_info['initialize_fcs'] = True
    new_netspec = create_net('deploy', new_net_info, datalayer_info)
    save_net(new_path, str(new_netspec.to_proto()))
    
    fcn8_net = caffe.Net(fcn8_path, fcn8_weights, caffe.TRAIN)
    new_net = caffe.Net(new_path, caffe.TRAIN)
    
    #transplant vgg-net conv weights
    transplant(new_net, fcn8_net, 'm')
    
    new_net.params['conv1_1m'][0].data[...] = new_net.params['conv1_1m'][0].data * 255
    new_net.params['conv1_1m'][1].data[...] = new_net.params['conv1_1m'][1].data * 255
    
    param_pairs = [('score-fr', 'score_fc'), ('score2', 'upscore2'), ('score-pool4', 'score_pool4'), ('score4', 'upscore_pool4'), ('score-pool3' ,'score_pool3'), ('upsample', 'upscore8')]
    if net_info['fc6_size'] == 4096 and net_info['fc7_size'] == 4096:
        #transplant fc6 weights
        new_net.params['fc6'][0].data[...] = 0
        new_net.params['fc6'][0].data[:, -512:][...] = fcn8_net.params['fc6'][0].data
        new_net.params['fc6'][1].data[...] = fcn8_net.params['fc6'][1].data
        param_pairs.append(('fc7', 'fc7'))

    #transplant the rest
    copy_params(new_net, fcn8_net, pair_param = param_pairs)
    
    new_net.save(new_weights)
    remove(new_path)
    
def deepmask_net_surgery(datalayer_info, net_info, clean):

    vgg_path = 'deepmask_net_tmp.prototxt'
    vgg_weights = '../data/fcn32s-heavy-pascal.caffemodel'
    new_weights = '../deepmask_net.caffemodel'
    
    if clean:
        silentremove(new_weights)
        return
        
    new_path = 'new_net_tmp.prototxt'
    new_net_info = net_info.copy()
    new_net_info['initialize_fcs'] = True
    vgg_netspec = create_vgg_net()
    new_netspec = create_net('deploy', new_net_info, datalayer_info)
    save_net(vgg_path, str(vgg_netspec.to_proto()))
    save_net(new_path, str(new_netspec.to_proto()))
    vgg_net = caffe.Net(vgg_path, vgg_weights, caffe.TRAIN)
    new_net = caffe.Net(new_path, caffe.TRAIN)
    
    #transplant vgg-net conv weights
    transplant(new_net, vgg_net, 'c')

    new_net.save(new_weights)
    remove(vgg_path)
    remove(new_path)

