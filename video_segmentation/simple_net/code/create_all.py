import caffe

from caffe import layers as L, params as P
from caffe.coord_map import coord_map_from_to
from surgery import *
import numpy as np
from net import create_net, save_net, silentremove
import string
import os.path as osp
import os

CAFFE_PATH = '/nethome/ashaban6/caffe/build/tools/caffe'
COCO_PATH = '/home/amir/coco'
PASCAL_PATH= '/home/amir/VOC2012'

#####################################################
def fcn8_solver_param(datalayer_name):
    solver_param = dict(momentum=.9,iter_size=1,weight_decay=5e-4, snapshot_iter=5e3, 
			weights_path='./fcn8_net.caffemodel',
			caffe_path=CAFFE_PATH, 
			resume_state_path='')
    if datalayer_name == 'coco':
	solver_param['max_iter'] = 700e3
	solver_param['lr_policy_step_size'] = 100e3
	solver_param['snapshot_iter'] = 1e4
	solver_param['base_lr'] = 1e-5
    elif datalayer_name[:5] == 'davis':
	solver_param['iter_size']=1
	solver_param['max_iter'] = 1e5
	solver_param['lr_policy_step_size'] = 10e3
	solver_param['base_lr'] = 1e-5
    return solver_param

def fcn8_net_info(initialize_fcs=False, datalayer_name = 'coco'):
    if datalayer_name == 'davis_sequence':
	datalayer_info = dict(name=datalayer_name, cur_shape = np.array([384,384])/ 2, next_shape = np.array([384,384]), batch_size=3, port = '6678', shuffle = True, max_len = 20, mean = (0.48501961, 0.45795686,0.40760392), bgr = False, scale_256 = False, bb1_enlargment = 1.1, bb2_enlargment = 2.2)
    else:
	datalayer_info = dict(name=datalayer_name, cur_shape = np.array([384,384])/ 2, next_shape = np.array([384,384]), batch_size=6,num_threads=2,max_queue_size = 8, mean = (0.48501961, 0.45795686,0.40760392), data_dir=COCO_PATH, noisy_mask = True, bgr = False, scale_256 = False, bb1_enlargment = 1.1, bb2_enlargment = 2.2)
    
    net_info = dict(name='fcn8',fc6_size=4096, fc7_size = 4096, initialize_fcs=initialize_fcs, surgeon=fcn8_net_surgery)  
    solver_param = fcn8_solver_param(datalayer_name)
    return (datalayer_info, net_info, solver_param)

########################################################

def simple_solver_param(datalayer_name):
    solver_param = dict(momentum=.9,iter_size=5,weight_decay=5e-4, snapshot_iter=5e3, 
			weights_path='./simple_net.caffemodel',
			caffe_path=CAFFE_PATH, 
			resume_state_path='')
    if datalayer_name == 'coco':
	solver_param['max_iter'] = 350e3
	solver_param['lr_policy_step_size'] = 100e3
	solver_param['base_lr'] = 1e-3
    elif datalayer_name[:5] == 'davis':
	solver_param['iter_size']=1
	solver_param['max_iter'] = 1e5
	solver_param['lr_policy_step_size'] = 10e3
	solver_param['base_lr'] = 1e-5
    return solver_param

def simple_net_info(initialize_fcs=False, datalayer_name = 'coco'):
    if datalayer_name == 'davis_sequence':
	datalayer_info = dict(name=datalayer_name, cur_shape = np.array([384,384])/ 2, next_shape = np.array([384,384]), batch_size=4, port = '6678', shuffle = True, max_len = 10, mean = (0.48501961, 0.45795686,0.40760392), bgr = False, scale_256 = False, bb1_enlargment = 1.1, bb2_enlargment = 2.2)
    else:
	datalayer_info = dict(name=datalayer_name, cur_shape = np.array([384,384])/ 2, next_shape = np.array([384,384]), batch_size=4,num_threads=2,max_queue_size = 8, mean = (0.48501961, 0.45795686,0.40760392), data_dir=COCO_PATH, noisy_mask = True, bgr = False, scale_256 = False, bb1_enlargment = 1.1, bb2_enlargment = 2.2)
    
    net_info = dict(name='simple',fc6_size=1024, fc7_size = 1024, initialize_fcs=initialize_fcs, surgeon=simple_net_surgery)  
    solver_param = simple_solver_param(datalayer_name)
    return (datalayer_info, net_info, solver_param)

#######################################################

def deepmask_solver_param(datalayer_name):
    solver_param = dict(momentum=.9,iter_size=1,weight_decay=5e-4, snapshot_iter=5e4, 
			weights_path='./deepmask_net.caffemodel',
			caffe_path=CAFFE_PATH, 
			resume_state_path='')
    if datalayer_name == 'coco':
	solver_param['max_iter'] = 1e6
	solver_param['lr_policy_step_size'] = 300e3
	solver_param['base_lr'] = 1e-3
    elif datalayer_name[:5] == 'davis':
	solver_param['max_iter'] = 10e3
	solver_param['lr_policy_step_size'] = 10e3
	solver_param['base_lr'] = 1e-5 
    return solver_param

def deepmask_net_info(initialize_fcs=False, datalayer_name = 'coco'):
    if datalayer_name == 'davis_sequence':
	datalayer_info = dict(name=datalayer_name, cur_shape = np.array([384,384])/ 2, next_shape = np.array([384,384]), batch_size=4, port = '6678', mean = (0.48501961, 0.45795686,0.40760392), noisy_mask = True, bgr = False, scale_256 = False, bb1_enlargment = 1.1, bb2_enlargment = 2.2)
    else:
	datalayer_info = dict(name=datalayer_name, cur_shape = np.array([256,256])/ 2, next_shape = np.array([256,256]), batch_size=24,num_threads=4,max_queue_size = 48, data_dir=COCO_PATH, bgr = False, scale_256 = False, bb1_enlargment = 1.1, bb2_enlargment = 2.2)
    net_info = dict(name='deepmask',fc6_size=1024, fc7_size = 1024, initialize_fcs=initialize_fcs, surgeon=deepmask_net_surgery, fc6_kernel=3, upscore_scale = 4)
    solver_param = deepmask_solver_param(datalayer_name)
    return (datalayer_info, net_info, solver_param)

##################################################################

def match_solver_param(datalayer_name, nrefinemnet_unit = 0):
    name = 'match'
    if nrefinemnet_unit > 0:
	name += str(nrefinemnet_unit)
    
    solver_param = dict(momentum=.9,iter_size=1,weight_decay=5e-4, snapshot_iter=5e3, 
			weights_path='./' + name +'_net.caffemodel',
			caffe_path=CAFFE_PATH, 
			resume_state_path='')
    if datalayer_name == 'coco':
	solver_param['max_iter'] = 700e3
	solver_param['lr_policy_step_size'] = 200e3
	solver_param['snapshot_iter'] = 1e4
	solver_param['base_lr'] = 1e-4
	
    elif datalayer_name == 'pascal':
	solver_param['max_iter'] = 700e3
	solver_param['lr_policy_step_size'] = 200e3
	solver_param['snapshot_iter'] = 1e4
	solver_param['base_lr'] = 1e-6
    elif datalayer_name[:5] == 'davis':
	solver_param['iter_size']=1
	solver_param['max_iter'] = 1e5
	solver_param['lr_policy_step_size'] = 10e3
	solver_param['base_lr'] = 1e-5
    return solver_param

def match_net_info(datalayer_name = 'coco', nrefinemnet_unit = 0, unsup_flow_loss = False):
    
    visible_flow = False
    #flow_scale = 0.05
    flow_scale = 1
    top_names = ['fg_image', 'next_image','label']
    if unsup_flow_loss:
        top_names += ['cur_masked_gt', 'next_masked_gt']
    if datalayer_name == 'davis_sequence':
	datalayer_info = dict(name=datalayer_name, cur_shape = np.array([384,384]), next_shape = np.array([384,384]), batch_size=2, port = '7792', shuffle = True, max_len = 100, mean = (0.40787054,  0.45752458,  0.48109378), bgr = True, scale_256 = True, bb1_enlargment = 2, bb2_enlargment = 2, flow_params = [], mask_mean = .5, mask_threshold = .5, flow_method = 'None', augmentations=['noisy_mask'], extra_steps=[-1, 2, -2, -4, 4], top_names = top_names)
    elif datalayer_name == 'coco':
	visible_flow = True
	datalayer_info = dict(name=datalayer_name, cur_shape = np.array([384,384]), next_shape = np.array([384,384]), batch_size=4,num_threads=3,max_queue_size = 8, mean = (0.40787054,  0.45752458,  0.48109378), data_dir=COCO_PATH, noisy_mask = True, bgr = True, scale_256 = True, bb1_enlargment = 2, bb2_enlargment = 2, inverse_flow=visible_flow, flow_scale=flow_scale)
    elif datalayer_name == 'pascal':
	visible_flow = True
	datalayer_info = dict(name=datalayer_name, cur_shape = np.array([384,384]), next_shape = np.array([384,384]), batch_size=4,num_threads=3,max_queue_size = 8, mean = (0.40787054,  0.45752458,  0.48109378), data_dir=PASCAL_PATH, noisy_mask = True, bgr = True, scale_256 = True, bb1_enlargment = 2, bb2_enlargment = 2, inverse_flow=visible_flow, flow_scale=flow_scale)
    
    name = 'match'
    if nrefinemnet_unit > 0:
	name += str(nrefinemnet_unit)
    net_info = dict(name=name,fc6_size=4096, fc7_size = 4096, surgeon=match_net_surgery, filler_param = {}, nrefinemnet_unit = nrefinemnet_unit, share_weights = False, visible_flow = visible_flow, flow_scale = flow_scale, unsup_flow_loss=unsup_flow_loss)
    solver_param = match_solver_param(datalayer_name, nrefinemnet_unit)
    return (datalayer_info, net_info, solver_param)


def epicmatch_solver_param(datalayer_name, nrefinemnet_unit = 0):
    name = 'epicmatch'
    if nrefinemnet_unit > 0:
	name += str(nrefinemnet_unit)
    
    solver_param = dict(momentum=.99,iter_size=1,weight_decay=5e-4, snapshot_iter=5e3, 
			weights_path='./' + name +'_net.caffemodel',
			caffe_path=CAFFE_PATH, 
			resume_state_path='')
    if datalayer_name == 'coco':
	solver_param['max_iter'] = 700e3
	solver_param['lr_policy_step_size'] = 200e3
	solver_param['snapshot_iter'] = 1e4
	solver_param['base_lr'] = 1e-4
	
    elif datalayer_name == 'pascal':
	solver_param['max_iter'] = 700e3
	solver_param['lr_policy_step_size'] = 200e3
	solver_param['snapshot_iter'] = 1e4
	solver_param['base_lr'] = 1e-6
    elif datalayer_name[:5] == 'davis':
	solver_param['iter_size']=1
	solver_param['max_iter'] = 1e6
	solver_param['lr_policy_step_size'] = 1e6
	solver_param['base_lr'] = 1e-6
    return solver_param

def epicmatch_net_info(datalayer_name = 'davis_sequence', nrefinemnet_unit = 0):
    bg_method = 'MASK'
    
    #name, down_scale, pad, flow_scale
    flow_params = [('iflow32', 32, None, 1.0)]
    for ref_id in range(nrefinemnet_unit):
	down_scale = 2**(4-ref_id)
	flow_params.append(('iflow' + str(down_scale), down_scale, None, 1.0))
    
    top_names = []
    if bg_method == 'FG':
        top_names = ['fg_image', 'next_image','label']
    elif bg_method == 'FG/BG':
        top_names = ['fg_image', 'bg_image', 'next_image', 'label']
    elif bg_method == 'MASK':
        top_names = ['current_image', 'current_mask', 'next_image','label']
    if datalayer_name == 'davis_sequence':
	datalayer_info = dict(name=datalayer_name, cur_shape = np.array([384,384]), next_shape = np.array([384,384]), batch_size=4, port = '7703', shuffle = True, max_len = 100, mean = (0.40787054,  0.45752458,  0.48109378), bgr = True, scale_256 = True, bb1_enlargment = 2, bb2_enlargment = 2, flow_params = flow_params, mask_mean = .5, mask_threshold = .5, flow_method = 'None', augmentations=['noisy_mask'], extra_steps=[-1, 2, -2, -4, 4], top_names = top_names)
    else:
	raise Exception('Not Implemented!')
    
    name = 'epicmatch'
    if nrefinemnet_unit > 0:
	name += str(nrefinemnet_unit)
    net_info = dict(name=name,fc6_size=4096, fc7_size = 4096, surgeon=epicmatch_net_surgery, filler_param = {}, nrefinemnet_unit = nrefinemnet_unit, share_weights = False, flow_params = flow_params, bg_method=bg_method)
    solver_param = epicmatch_solver_param(datalayer_name, nrefinemnet_unit)
    return (datalayer_info, net_info, solver_param)
#################################################################

def parse_file(input_path, output_path, dictionary):
    with open(input_path, 'r') as in_file:
	with open(output_path, 'w') as out_file:
	    data = string.Template(in_file.read())
	    out_file.write(data.substitute(**dictionary))

def create_files(datalayer_info, net_info, solver_param, create_caffemodel = False, clean = False):
    
    splits = ['train']
    
    suffix = datalayer_info['name'] + '_' + net_info['name']
    
    write_path = osp.join('..', suffix)
    os.makedirs(osp.join(write_path, 'snapshots'))
    os.mkdir(osp.join(write_path, 'log'))

    if create_caffemodel and net_info.has_key('surgeon') and net_info['surgeon'] is not None:
	#Do Surgery to get parameters
    	net_info['surgeon'](datalayer_info, net_info, clean)
    
    #create net
    for split in splits:
	file_name = osp.join(write_path, split + '_' + suffix + '.prototxt')
	if clean:
	    silentremove(file_name)
	else:
	    net_spec = create_net(split, net_info, datalayer_info)
	    save_net(file_name, str(net_spec.to_proto()))
    
    solver_path = 'solver_' + suffix + '.prototxt'
    train_path = 'train_' + suffix + '.sh'
    resume_path = 'resume_' + suffix + '.sh'
    if clean:
	silentremove(write_path + solver_path)
	silentremove(write_path + train_path)
	silentremove(write_path + resume_path)
    else:
	#create solver$suffix.prototxt
	final_iter_size =  solver_param['iter_size'] * datalayer_info['batch_size']
	solver_dict = dict(TRAIN_NET= 'train_' + suffix + '.prototxt', 
		       BASE_LR = solver_param['base_lr'],
		       STEP_SIZE= int(solver_param['lr_policy_step_size'] / final_iter_size), 
		       MOMENTUM= solver_param['momentum'], 
		       ITER_SIZE= solver_param['iter_size'], 
		       MAX_ITER= int(solver_param['max_iter'] / final_iter_size), 
		       WEIGHT_DECAY= solver_param['weight_decay'], 
		       SNAPSHOT= int(solver_param['snapshot_iter'] / final_iter_size),
		       SNAPSHOT_PREFIX= './snapshots/' + suffix)
	
	#create train$suffix.sh
	train_dict = dict(CAFFE_PATH=solver_param['caffe_path'],SOLVER=solver_path,WEIGHTS=solver_param['weights_path'], LOG_NAME='train_' + suffix)
	
	#create resume`$suffix.sh
	resume_dict = dict(CAFFE_PATH=solver_param['caffe_path'], SOLVER=solver_path, SNAPSHOT=solver_param['resume_state_path'], LOG_NAME='resume_' + suffix)
	
	parse_file('../data/solver.prototxt', osp.join(write_path, solver_path), solver_dict)
	parse_file('../data/train.sh', osp.join(write_path, train_path), train_dict)
	parse_file('../data/resume.sh', osp.join(write_path, resume_path), resume_dict)
if __name__ == '__main__':    
    net_info_param = dict(nrefinemnet_unit=2, unsup_flow_loss=False)
    net_info_retriver = match_net_info
    #net_info_retriver = match_net_info
    clean = False
    datalayer_info, net_info, solver_param = net_info_retriver(datalayer_name = 'davis_sequence', **net_info_param)
    create_files(datalayer_info, net_info, solver_param, True, clean)

