import numpy as np
from transformer import Transformer_dist

COCO_PATH = '/home/amir/coco'
PASCAL_PATH = '/home/amir/VOC2012'
COCO_CATS = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 
                             'bus', 'train', 'truck', 'boat', 'bird', 'cat', 'dog', 
                             'horse', 'sheep', 'cow', 'elephant', 'bear', 'zebra', 
                             'giraffe', 'kite']

PASCAL_CATS = ['aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 
                               'bus', 'car' , 'cat', 'chair', 'cow',
                               'dog', 'horse', 'motorbike', 'person',
                               'sheep', 'sofa', 'train']
sqrt2 = np.sqrt(2)



class Map(dict):
    """
    Example:
    m = Map({'first_name': 'Eduardo'}, last_name='Pool', age=24, sports=['Soccer'])
    """
    def __init__(self, *args, **kwargs):
        super(Map, self).__init__(*args, **kwargs)
        for arg in args:
            if isinstance(arg, dict):
                for k, v in arg.iteritems():
                    self[k] = v

        if kwargs:
            for k, v in kwargs.iteritems():
                self[k] = v

    def __getattr__(self, attr):
        return self.get(attr)

    def __setattr__(self, key, value):
        self.__setitem__(key, value)

    def __setitem__(self, key, value):
        super(Map, self).__setitem__(key, value)
        self.__dict__.update({key: value})

    def __delattr__(self, item):
        self.__delitem__(item)

    def __delitem__(self, key):
        super(Map, self).__delitem__(key)
        del self.__dict__[key]
        
        
profile_1 = Map(
                video_base_trans=Transformer_dist({'transx_param':(0.0,0.0), 'transy_param':(0.0,0.0), 'rot_param':(0.0, 0.0), 
                                                   'zoomx_param':(1.0, 0.0), 'zoomy_param':(1.0, 0.0), 'shear_param':(0.0, 2)}, {'sigma_range':(.0, .1), 'gamma_range':(.9, 1.1),
                                                   'contrast_range':(.8, 1.2), 'brightness_sigma':.1, 'mult_rgb_range':(0.9, 1.1),
                                                   'blur_param':[(.95, 0), (.02, 2), (.02, 3), (.01, 5)]}),
                video_frame_trans=Transformer_dist({'transx_param':(0.0,0.27035499630719317/sqrt2/4), 'transy_param':(0.0,0.03967734490564475/sqrt2/4), 'rot_param':(0.0, 5/4), 
                                                   'zoomy_param':(1.0, 0.1625625379509229/sqrt2/4), 'zoomx_param':(1.0, 0.11389299050167503/sqrt2/4), 'shear_param':(0.0, 1)}, {'sigma_range':(.0, .05), 'gamma_range':(.95, 1.05),
                                                   'contrast_range':(.90, 1.10), 'brightness_sigma':.01, 'mult_rgb_range':(0.95, 1.05),
                                                   'blur_param':[(.95, 0), (.02, 2), (.02, 3), (.01, 5)]}), 
                                                   
                image_base_trans=Transformer_dist({'transx_param':(0.0,0.0), 'transy_param':(0.0,0.0), 'rot_param':(0.0, 0.0), 
                                                   'zoomx_param':(1.0, 0.0), 'zoomy_param':(1.0, 0.0), 'shear_param':(0.0, 2)}, {'sigma_range':(.0, .1), 'gamma_range':(.9, 1.1),
                                                   'contrast_range':(.8, 1.2), 'brightness_sigma':.1, 'mult_rgb_range':(0.9, 1.1),
                                                   'blur_param':[(.95, 0), (.02, 2), (.02, 3), (.01, 5)]}),
                image_frame_trans=Transformer_dist({'transx_param':(0.0,0.27035499630719317/sqrt2), 'transy_param':(0.0,0.03967734490564475/sqrt2), 'rot_param':(0.0, 5), 
                                                   'zoomy_param':(1.0, 0.1625625379509229/sqrt2), 'zoomx_param':(1.0, 0.11389299050167503/sqrt2), 'shear_param':(0.0, 1)}, {'sigma_range':(.0, .05), 'gamma_range':(.95, 1.05),
                                                   'contrast_range':(.90, 1.10), 'brightness_sigma':.01, 'mult_rgb_range':(0.95, 1.05),
                                                   'blur_param':[(.95, 0), (.02, 2), (.02, 3), (.01, 5)]}), 
                top_names = ['current_image', 'fg_image', 'bg_image', 'next_image','current_mask','label'],
                next_shape=(384, 384),
                cur_shape=(384, 384),
                shuffle=True,
                video_setting={'step_param':(-4, 4), 'offset_param':(0, 10), 'length_param':(50, 50)},
                bgr=True,
                scale_256=True,
                bb1_enlargment=2,
                bb2_enlargment=2,
                mask_mean = .5, 
                mean = (0.40787054, 0.45752458, 0.48109378), 
                mask_threshold = 0.5, 
                augmentations = ['noisy_mask'], 
                result_dir = '',
                batch_size = 1,
                port = '',
                flow_params = [],
                flow_method = 'None',
                video_sets=['training', 'training_segtrackv2', 'training_jumpcut'],
                image_sets=['pascal-test', 'pascal-training'],
                areaRng = [1500.0 , np.inf],
                pascal_cats = PASCAL_CATS,
                coco_cats = COCO_CATS,
                coco_path = COCO_PATH,
                pascal_path = PASCAL_PATH
               )

profile_2 = Map(
                video_base_trans=None,
                video_frame_trans=None,
                image_base_trans=None,
                image_frame_trans=None, 
                top_names = ['current_image', 'fg_image', 'bg_image', 'next_image','current_mask','label'],
                next_shape=(384, 384),
                cur_shape=(384, 384),
                shuffle=False,
                video_setting={'step_param':(1, 1), 'offset_param':(0, 0), 'length_param':(np.inf, np.inf)},
                bgr=True,
                scale_256=True,
                bb1_enlargment=2,
                bb2_enlargment=2,
                mask_mean = .5, 
                mean = (0.40787054, 0.45752458, 0.48109378), 
                mask_threshold = 0.5, 
                augmentations = [], 
                result_dir = '',
                batch_size = 1,
                port = '',
                flow_params = [],
                flow_method = 'None',
                video_sets=['test'],
                image_sets = [],
                areaRng = [1500.0 , np.inf],
                pascal_cats = PASCAL_CATS,
                coco_cats = COCO_CATS,
                coco_path = COCO_PATH,
                pascal_path = PASCAL_PATH
               )

profile_yt = Map(
                video_base_trans=None,
                video_frame_trans=None,
                image_base_trans=None,
                image_frame_trans=None, 
                top_names = ['current_image', 'fg_image', 'bg_image', 'next_image','current_mask','label'],
                next_shape=(384, 384),
                cur_shape=(384, 384),
                shuffle=False,
                video_setting={'step_param':(1, 1), 'offset_param':(0, 0), 'length_param':(np.inf, np.inf)},
                bgr=True,
                scale_256=True,
                bb1_enlargment=2,
                bb2_enlargment=2,
                mask_mean = .5, 
                mean = (0.40787054, 0.45752458, 0.48109378), 
                mask_threshold = 0.5, 
                augmentations = [], 
                result_dir = '',
                batch_size = 1,
                port = '',
                flow_params = [],
                flow_method = 'None',
                video_sets=['test_youtube'],
                image_sets = [],
                areaRng = [1500.0 , np.inf],
                pascal_cats = PASCAL_CATS,
                coco_cats = COCO_CATS,
                coco_path = COCO_PATH,
                pascal_path = PASCAL_PATH
               )


profile_tmp = Map(
                video_base_trans=None,
                video_frame_trans=None,
                image_base_trans=None,
                image_frame_trans=None, 
                next_shape=(384, 384),
                cur_shape=(384, 384),
                shuffle=False,
                video_setting={'step_param':(1, 1), 'offset_param':(0, 0), 'length_param':(np.inf, np.inf)},
                top_names = ['current_image', 'fg_image', 'bg_image', 'next_image','current_mask','cur_masked_gt', 'next_masked_gt', 'label'],
                bgr=True,
                scale_256=True,
                bb1_enlargment=2,
                bb2_enlargment=2,
                mask_mean = .5, 
                mean = (0.40787054, 0.45752458, 0.48109378), 
                mask_threshold = 0.5, 
                augmentations = ['noisy_mask'], 
                result_dir = '',
                batch_size = 1,
                port = '',
                flow_params = [],
                flow_method = 'None',
                video_sets=['test'],
                image_sets = [],
                areaRng = [1500.0 , np.inf],
                pascal_cats = PASCAL_CATS,
                coco_cats = COCO_CATS,
                coco_path = COCO_PATH,
                pascal_path = PASCAL_PATH
               )





