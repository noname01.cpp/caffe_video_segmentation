import json
import time
import caffe
import numpy as np
import cPickle as pickle
import random
from multiprocessing import Process, Queue, Pool
from davis import cfg
import zmq
import sys
import traceback
import itertools
import util
from util import cprint, bcolors, bbox, crop, crop_undo, add_noise_to_mask
from operator import itemgetter 
from functools import partial
from skimage.transform import resize
import copy

import os.path as osp
import os
import multiprocessing
import multiprocessing.pool
from skimage.io import imsave

class Video:
    def __init__(self, player = None, seq_index = -1, frame_index = sys.maxint):
        self.player = player
        self.frame_index = frame_index
        self.predicted_mask = None
        self.mask_crop_param = None
        
    def is_finished(self):
        return self.player is None or self.frame_index >= (self.player.length-2)

class SequenceLoaderProcess(Process):
    def __init__(self, name=None, args=(), kwargs=None):
        Process.__init__(self, name=name)
        self.params = kwargs['params']
        self.batch_size = self.params['batch_size']
        self.port = self.params['port']
        self.result_dir = self.params['result_dir']
        self.queue = kwargs['queue']
        self.loader = kwargs['loader']
        self.save_result = (self.result_dir is not None and self.result_dir != '')
        self.videos = [Video() for i in xrange(self.batch_size)]
        self.next_seq_index = 0
        self.load_items()
        self.mutex = multiprocessing.Lock()
        self.firstTime = True
        
    def run(self):
        try:
            #Initiate socket
            self.context = zmq.Context.instance()
            self.sock = self.context.socket(zmq.REP)
            self.sock.connect('tcp://localhost:' + self.port)
            cprint ('Server started', bcolors.OKGREEN)
            next_frame_reader = partial(SequenceLoaderProcess.load_next_frame, self)
            while True:
                load_new_indices = [i for i in range(self.batch_size) if self.videos[i].is_finished()]
                load_next_indices = [i for i in range(self.batch_size) if not self.videos[i].is_finished()]
                items = [None] * self.batch_size
                if self.save_result:
                    video_copy = [copy.copy(video) for video in self.videos]
                    
                #1) Those frames that do not require previous prediction will be loaded immediately
                if len(load_new_indices) > 1:
                    pool = multiprocessing.pool.ThreadPool(len(load_new_indices))
                    new_items = pool.map(next_frame_reader, load_new_indices)
                    pool.close()
                    pool.join()
                    for i in range(len(new_items)):
                        items[load_new_indices[i]] = new_items[i]
                else:
                    for i in load_new_indices:
                        items[i] = next_frame_reader(i)
                    
                
                #2) Read the predicted masks and compute the rest
                #check if we have a message to read
                if not self.firstTime:
                    predicted_mask = self.read_message()
                    for i in load_next_indices:
                        self.videos[i].predicted_mask = predicted_mask[i]
                
                if len(load_next_indices) > 1:
                    pool = multiprocessing.pool.ThreadPool(len(load_next_indices))
                    next_items = pool.map(next_frame_reader, load_next_indices)
                    pool.close()
                    pool.join()
                    for i in range(len(next_items)):
                        items[load_next_indices[i]] = next_items[i]
                else:
                    for i in load_next_indices:
                        items[i] = next_frame_reader(i)
                            
                
                for i in range(self.batch_size):
                    self.queue.put(items[i])
                
                if self.save_result:
                    for i in range(self.batch_size):
                        #Save Initialization mask
                        if self.videos[i].frame_index == 0:
                            final_mask_dir = osp.join(self.result_dir, 'final', self.videos[i].player.name)
                            soft_mask_dir = osp.join(self.result_dir, 'soft', self.videos[i].player.name)
                            if not osp.exists(final_mask_dir):
                                os.makedirs(final_mask_dir)
                            if not osp.exists(soft_mask_dir):    
                                os.makedirs(soft_mask_dir)
                            small_mask = items[i]['current_mask']
                            if self.loader.scale_256:
                                small_mask = small_mask / 255.0 + self.loader.mask_mean
                            p_mask = crop_undo(small_mask, **self.videos[i].mask_crop_param)
                            imsave(osp.join(soft_mask_dir, '%05d.png' % self.videos[i].frame_index), p_mask)
                            p_mask[p_mask < .5] = 0
                            p_mask[p_mask > 0] = 1
                            #p_mask = small_mask
                            imsave(osp.join(final_mask_dir, '%05d.png' % self.videos[i].frame_index), p_mask)
 
			#Save Segmentation Results
                        if not self.firstTime:
                            final_mask_dir = osp.join(self.result_dir, 'final', video_copy[i].player.name)
                            soft_mask_dir = osp.join(self.result_dir, 'soft', video_copy[i].player.name)
                            p_mask = crop_undo(predicted_mask[i], **video_copy[i].mask_crop_param)
                            imsave(osp.join(soft_mask_dir, '%05d.png' % (video_copy[i].frame_index + 1)), p_mask)
                            p_mask[p_mask < .5] = 0
                            p_mask[p_mask > 0] = 1
                            #p_mask = predicted_mask[i]
                            imsave(osp.join(final_mask_dir, '%05d.png' % (video_copy[i].frame_index + 1)), p_mask)
                                                
                        #Save Images
                        image_dir = osp.join(self.result_dir, 'image', self.videos[i].player.name)
                        if not osp.exists(image_dir):
                            os.makedirs(image_dir)
			frame = self.videos[i].player.get_frame(self.videos[i].frame_index)
			image = frame['image']
                        imsave(osp.join(image_dir, '%05d.png' % self.videos[i].frame_index), image)
                        
                        if self.videos[i].is_finished():
                            if not osp.exists(image_dir):
                                os.makedirs(image_dir)
                            frame = self.videos[i].player.get_frame(self.videos[i].frame_index + 1)
                            image = frame['image']
                            imsave(osp.join(image_dir, '%05d.png' % (self.videos[i].frame_index + 1)), image)
                        
                self.firstTime = False
        except:
            cprint ('An Error Happended in run()',bcolors.FAIL)
            cprint (str("".join(traceback.format_exception(*sys.exc_info()))), bcolors.FAIL)
            self.queue.put(None)
            raise Exception("".join(traceback.format_exception(*sys.exc_info())))
    
    def load_next_frame(self, i):
        val = None
        while val is None:
            if self.videos[i].is_finished():
                self.mutex.acquire()
                self.update_video(i)
                self.mutex.release()
            else:
                self.videos[i].frame_index += 1
            try:
                val = self.loader.load_frame(self.videos[i].player, self.videos[i].frame_index, self.videos[i].predicted_mask, self.videos[i].mask_crop_param)
            except Exception as e:
                info = '\nwhen reading ' + self.videos[i].player.name + ' ' + str(self.videos[i].frame_index)
                raise type(e), type(e)(e.message + info), sys.exc_info()[2]

            if val is None:
                self.videos[i].mask_crop_param = None
                self.videos[i].predicted_mask = None
        
        self.videos[i].predicted_mask = None
        self.videos[i].mask_crop_param = val['label_crop_param']
        return val
    
    def update_video(self, i):
        #create next player
        db_item = self.db_items[self.next_seq_index]
        #cprint('Update Video ' + str(self.next_seq_index) + ' ' + str(db_item), bcolors.OKBLUE)
        if isinstance(db_item, util.DBImageItem):
            base_trans = None if self.params['image_base_trans'] is None else self.params['image_base_trans'].sample()
            compute_iflow = len(self.params['flow_params']) > 0
            player = util.ImagePlayer(db_item, base_trans, self.params['image_frame_trans'], compute_iflow, 2)
        elif isinstance(db_item, util.DBDAVISItem):
            base_trans = None if self.params['video_base_trans'] is None else self.params['video_base_trans'].sample()
            flo_method = self.params['flow_method']
            step = 0
            while step == 0:
                step = random.randint(*self.params['video_setting']['step_param'])
            offset = random.randint(*self.params['video_setting']['offset_param'])
            len_params = self.params['video_setting']['length_param']
            if np.isinf(len_params[0]) or np.isinf(len_params[1]):
                max_len = np.inf
            else:
                max_len = random.randint(*self.params['video_setting']['length_param'])
            #cprint('New Video Player: ' + str(offset) + ':' + str(max_len) + ':' + str(step), bcolors.OKBLUE)
            player = util.VideoPlayer(db_item, base_trans, self.params['video_frame_trans'], step = step, offset = offset, max_len = max_len, flo_method = flo_method)
        else:
            raise Exception
        #
        self.videos[i].player = player
        self.videos[i].frame_index = 0
        self.videos[i].predicted_mask = None
        self.videos[i].mask_crop_param = None
        self.next_seq_index = self.next_seq_index + 1
        if self.next_seq_index >= len(self.db_items):
            self.next_seq_index = 0
            if self.params['shuffle']:
                random.shuffle(self.db_items)
        if player.length < 2:
            self.update_video(i)

    def read_message(self):
        cprint('server before receive', bcolors.WARNING)
        message = self.sock.recv_pyobj()
        cprint('server received message ' + str(message.shape), bcolors.WARNING)
        self.sock.send('OK')
        return message
           
    def load_items(self):
        self.db_items = []
        if self.params.has_key('video_sets'):
            self.db_items.extend(util.DAVIS().getItems(self.params['video_sets']))
        if self.params.has_key('image_sets'):
            for image_set in self.params['image_sets']:
                if image_set.startswith('pascal'):
                    self.db_items.extend(util.PASCAL(self.params['pascal_path'], image_set[7:]).getItems(self.params['pascal_cats'], self.params['areaRng']))
                elif image_set.startswith('coco'):
                    self.db_items.extend(util.COCO(self.params['coco_path'], image_set[5:]).getItems(self.params['coco_cats'], self.params['areaRng']))
                else:
                    raise Exception
        if self.params['shuffle']:
            random.shuffle(self.db_items)
        cprint('Total of ' + str(len(self.db_items)) + ' db items loaded!', bcolors.OKBLUE)
        
class SequenceLoader(object):
    def __init__(self, params):
        self.resizeShape1 = params['cur_shape']
        self.resizeShape2 = params['next_shape']
        self.bgr = params['bgr']
        self.flow_params = params['flow_params']
        self.scale_256 = params['scale_256']
        self.bb1_enlargment = params['bb1_enlargment']
        self.bb2_enlargment = params['bb2_enlargment']
        self.mask_threshold = params['mask_threshold']
        self.test_mode = (params['result_dir'] is not None and params['result_dir'] != '')
        self.noisy_mask = ('noisy_mask' in params['augmentations'])
        self.mask_mean = params['mask_mean']
        self.top_names = params['top_names']
        self.mean = np.array(params['mean']).reshape(1,1,3)
        assert (len(self.flow_params) == 0) or (self.resizeShape1 == self.resizeShape2 and self.bb1_enlargment == self.bb2_enlargment)
        if self.bgr:
            #Always store mean in RGB format
            self.mean = self.mean[:,:, ::-1]
        
    def load_frame(self, player, frame, mask1_cropped, mask1_crop_param):
        cprint('FRAME = ' + str(frame), bcolors.WARNING)
        
        #reading first frame
        fresh_mask = True
        frame1_dict = player.get_frame(frame)
        image1 = frame1_dict['image']
        orig_mask1 = frame1_dict['mask']
        mask1 = orig_mask1
        if mask1 is None and self.test_mode and mask1_cropped is not None and mask1_crop_param is not None:
            mask1 = orig_mask1 = np.ones(image1.shape[:2], dtype=np.float32)
        if (mask1 > .5).sum() < 200:
                return None
        if mask1_cropped is not None and mask1_crop_param is not None:
            #convert mask1 to its original shape using mask1_crop_param
            uncrop_mask1 = crop_undo(mask1_cropped, **mask1_crop_param)
            inter = np.logical_and((mask1 > .5), uncrop_mask1 > .5).sum()
            union = np.logical_or((mask1 > .5), uncrop_mask1 > .5).sum()
            
            if float(inter)/union > .1 or self.test_mode:
                mask1 = uncrop_mask1
                fresh_mask = False
        
        #reading second frame
        frame2_dict = player.get_frame(frame + 1, len(self.flow_params) > 0)
        image2 = frame2_dict['image']
        mask2 = frame2_dict['mask']
        if mask2 is None and self.test_mode:
            mask2 = mask1.copy()
        if not frame2_dict.has_key('iflow'):
            frame2_dict['iflow'] = np.zeros((image2.shape[0], image2.shape[1], 2))
        
        # Cropping and resizing
        filtered_mask1 = mask1.copy()
        filtered_mask1[filtered_mask1 < self.mask_threshold] = 0
        
        m = 0
        while self.test_mode and (np.abs(filtered_mask1)).sum() < 50 and (self.mask_threshold - m) >= 0:
            m += .05
            filtered_mask1 = mask1.copy()
            filtered_mask1[filtered_mask1 < (self.mask_threshold - m)] = 0
        
        mask1_bbox = bbox(filtered_mask1)
        if (mask1_bbox[1] - mask1_bbox[0]) <= 0 or (mask1_bbox[3] - mask1_bbox[2]) <= 0:
		raise Exception('Lost the object. Test mode: ' + str(self.test_mode))
        cimg = crop(image1, mask1_bbox, bbox_enargement_factor = self.bb1_enlargment, output_shape = self.resizeShape1, resize_order = 3) - self.mean
        cmask = crop(mask1.astype('float32'), mask1_bbox, bbox_enargement_factor = self.bb1_enlargment, output_shape = self.resizeShape1)
        if self.noisy_mask and fresh_mask:
            #print 'Adding Noise to the mask...'
            cmask = add_noise_to_mask(cmask)
        cimg_masked = cimg * (cmask[:,:,np.newaxis] > self.mask_threshold)
        cimg_bg = cimg * (cmask[:,:,np.newaxis] <= self.mask_threshold)
        nimg = crop(image2, mask1_bbox, bbox_enargement_factor = self.bb2_enlargment, output_shape = self.resizeShape2, resize_order = 3) - self.mean
        label = crop(mask2.astype('float32'), mask1_bbox, bbox_enargement_factor = self.bb2_enlargment, output_shape = self.resizeShape2, resize_order = 0)
        label_crop_param = dict(bbox=mask1_bbox, bbox_enargement_factor=self.bb2_enlargment, output_shape=image1.shape[0:2])
        
        
        corig_mask = crop(orig_mask1.astype('float32'), mask1_bbox, bbox_enargement_factor = self.bb1_enlargment, output_shape = self.resizeShape1)
        
        
        cn_masked_gt = (nimg + self.mean) * label[:, :, np.newaxis]
        cc_masked_gt = (cimg + self.mean) * corig_mask[:, :, np.newaxis]
        
        cmask -= self.mask_mean
        if self.bgr:
            cimg = cimg[:,:, ::-1]
            cimg_masked = cimg_masked[:, :, ::-1]
            cimg_bg = cimg_bg[:, :, ::-1]
            nimg = nimg[:, :, ::-1]
	    #cn_masked_gt = cn_masked_gt[:, :, ::-1]
            #cc_masked_gt = cc_masked_gt[:, :, ::-1]
        if self.scale_256:
            cimg *= 255
            cimg_masked *= 255
            cimg_bg *= 255
            nimg *= 255
            cmask *= 255
	    #cn_masked_gt *= 255
	   #cc_masked_gt *= 255

            
        item = {'current_image': cimg.transpose((2,0,1)),
                'fg_image' : cimg_masked.transpose((2,0,1)),
                'bg_image' : cimg_bg.transpose((2,0,1)),
                'current_mask' : cmask,
                'next_image'   :nimg.transpose((2,0,1)),
                'cur_masked_gt': cc_masked_gt.transpose((2,0,1)),
                'next_masked_gt': cn_masked_gt.transpose((2,0,1)),
                'label'        : label,
                'label_crop_param' : label_crop_param}
        
        #crop inv_flow
        if len(self.flow_params) > 0:
            inv_flow = frame2_dict['iflow']
            max_val = np.abs(inv_flow).max()
            if max_val != 0:
                inv_flow /= max_val
            iflow = crop(inv_flow, mask1_bbox, bbox_enargement_factor = self.bb2_enlargment, resize_order=1, output_shape = self.resizeShape2, clip = False, constant_pad = 0)
            
            x_scale = float(iflow.shape[1])/(mask1_bbox[3] - mask1_bbox[2] + 1)/self.bb2_enlargment
            y_scale = float(iflow.shape[0])/(mask1_bbox[1] - mask1_bbox[0] + 1)/self.bb2_enlargment
            
            for i in range(len(self.flow_params)):
                name, down_scale, offset, flow_scale = self.flow_params[i]
                pad = int(-offset + (down_scale - 1)/2)
                h = np.floor(float(iflow.shape[0] + 2 * pad) / down_scale)
                w = np.floor(float(iflow.shape[1] + 2 * pad) / down_scale)
                n_flow = np.pad(iflow, ((pad, int(h * down_scale - iflow.shape[0] - pad)), (pad, int(h * down_scale - iflow.shape[1] - pad)), (0,0)), 'constant')
                n_flow = resize( n_flow, (h,w), order = 1, mode = 'nearest', clip = False)
                n_flow[:, :, 0] *= max_val * flow_scale * x_scale / down_scale
                n_flow[:, :, 1] *= max_val * flow_scale * y_scale / down_scale
                
                n_flow = n_flow.transpose((2,0,1))[::-1, :, :]
                item[name] = n_flow
        return item

class DavisDataLayerServer(caffe.Layer): 
    def __del__(self):
        self.process.terminate()

    def setup(self, bottom, top):
        params = eval(self.param_str)
                
        if params.has_key('profile'):
            settings = __import__('settings')
            profile = getattr(settings, params['profile'])
            profile.update(params)
            params = profile
        
        
        self.all_top_names = {'current_image':(3,) + params['cur_shape'], 
                              'fg_image':(3,) + params['cur_shape'], 
                              'bg_image':(3,) + params['cur_shape'], 
                              'next_image':(3,) + params['next_shape'],
                              'current_mask':(1,) + params['cur_shape'], 
                              'cur_masked_gt':(3,) + params['cur_shape'],
                              'next_masked_gt':(3,) + params['cur_shape'], 
                              'label':(1,) + params['next_shape']}
        if params.has_key('top_names'):
            self.top_names = params['top_names']
        else:
            self.top_names = self.all_top_names.keys()
            params['top_names'] = self.top_names
        
        self.batch_size = params['batch_size']
        self.queue = Queue(self.batch_size)
        self.process = SequenceLoaderProcess(kwargs={'queue':self.queue,'loader':SequenceLoader(params),'params':params})
        self.process.daemon = True
        self.process.start()
        
        
        for i in range(len(self.top_names)):
            top_name = self.top_names[i]
            assert top_name in self.all_top_names.keys()
            top[i].reshape(self.batch_size, *self.all_top_names[top_name])
        
        flow_start = len(self.top_names)
        for i in range(len(params['flow_params'])):
            ##in out network we have flow_coordinate = down_scale * 'name'_coordinate + offset
            ##in python resize we have flow_coordinate = down_scale * 'name'_coordinate + (down_scale - 1)/2 - pad
            ## ==> (down_scale - 1)/2 - pad = offset ==> pad = -offset + (down_scale - 1)/2
            
            name, down_scale, offset, flow_scale = params['flow_params'][i]
            pad = -offset + (down_scale - 1)/2
            #assert pad == int(pad) and pad >= 0 and offset <= 0
            assert pad == int(pad) and pad >= 0
            h = int(np.floor(float(params['next_shape'][0] + 2 * pad) / down_scale))
            w = int(np.floor(float(params['next_shape'][1] + 2 * pad) / down_scale))
            top[i + flow_start].reshape(self.batch_size, 2, h, w)
            self.top_names.append(name)
            
    def forward(self, bottom, top):
        cprint ('Queue size ' + str(self.queue.qsize()), bcolors.OKGREEN)
        for itt in range(self.batch_size):
            item = self.queue.get()
            if item is None:
                self.process.terminate()
                raise Exception
            for i in range(len(self.top_names)):
                top[i].data[itt,...] = item[self.top_names[i]]
                
    def reshape(self, bottom, top):
        pass

    def backward(self, top, propagate_down, bottom):
        pass
