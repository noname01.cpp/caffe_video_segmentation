import caffe
import numpy as np
from matplotlib.pyplot import imsave, imshow, show, figure, hist, imread
import net
import create_all
from caffe.coord_map import coord_map_from_to, crop
from caffe import layers as L, params as P
import surgery
import Image
def plot_result(net, i = 0):
    mean = np.array((0.40787054, 0.45752458, 0.48109378)).reshape((3,1,1)) * 255
    a = net.blobs['next_im'].data[i]
    b = a + mean
    c = b.transpose((1,2,0))[:,:,::-1]
    c = c/255
    imshow(c)
    
    figure()
    disp = net.blobs['visible_disp_m5'].data[i]
    blend = .1
    normalized_dispx = (disp.transpose((1,2,0))[:, :, 1][:, :, np.newaxis] - disp[1].min()) / (disp[1].max() - disp[1].min())
    new_img = c * blend + normalized_dispx * (1 - blend)
    imshow(new_img, interpolation='none')
    
    figure()
    disp = net.blobs['visible_disp_m5'].data[i]
    imshow(disp[1], interpolation='none')
    
    
    #
    figure()
    disp = net.blobs['iflow'].data[i]
    disp[np.isnan(disp)] = 0
    imshow(disp[1], interpolation='none')
    #
    
    figure()
    d = net.blobs['masked_im'].data[i]
    e = d + mean
    f = e.transpose((1,2,0))[:,:,::-1]
    imshow(f/255)
    
    show()

    
model = '../train_coco_match.prototxt'
weights = '../snapshots/coco_match_iter_126473.caffemodel'

caffe.set_mode_gpu()    
net = caffe.Net(model, weights, caffe.TRAIN)

net.forward()
plot_result(net)

