import caffe
import numpy as np
from matplotlib.pyplot import imshow, show, figure
from skimage import io
import util
import time

new_path = 'test_davis_server.prototxt'    
new_net = caffe.Net(new_path, caffe.TRAIN)
mean = np.array((0.40787054, 0.45752458, 0.48109378))[::-1].reshape((1, 1, 1, 3)) * 255
for i in range(20000):
  print 'iteration', i 
  if (i % 25) == 0:
      time.sleep(1)
  new_net.forward()
  if (i % 10) == 0:
      cur_im = new_net.blobs['cur_im'].data.transpose((0, 2, 3, 1))[:, :, :, ::-1] + mean
      masked_im = new_net.blobs['masked_im'].data.transpose((0, 2, 3, 1))[:, :, :, ::-1] + mean
      next_im = new_net.blobs['next_im'].data.transpose((0, 2, 3, 1))[:, :, :, ::-1] + mean
      cur_masked_gt = new_net.blobs['cur_masked_gt'].data.transpose((0, 2, 3, 1))[:, :, :, ::-1] + mean
      next_masked_gt = new_net.blobs['cur_masked_gt'].data.transpose((0, 2, 3, 1))[:, :, :, ::-1] + mean
      label = new_net.blobs['label'].data
      f32 = new_net.blobs['f32'].data.transpose((0,2,3,1))[:, :, :, ::-1]
      f4 = new_net.blobs['f4'].data.transpose((0,2,3,1))[:, :, :, ::-1]
      #print cur_im.shape, masked_im.shape, next_im.shape, f32.shape, f4.shape
      for b in range(cur_im.shape[0]):
	  name = './data/%d_%dcur_im.png' % (i, b)
	  io.imsave(name, (cur_im[b]).astype('uint8'))
	  name = './data/%d_%dmasked_im.png' % (i, b)
	  io.imsave(name, (masked_im[b]).astype('uint8'))
	  name = './data/%d_%dnext_im.png' % (i, b)
	  io.imsave(name, (next_im[b]).astype('uint8'))
	  name = './data/%d_%dcur_masked_gt.png' % (i, b)
          io.imsave(name, (cur_masked_gt[b]).astype('uint8'))
          name = './data/%d_%dnext_masked_gt.png' % (i, b)
          io.imsave(name, (next_masked_gt[b]).astype('uint8'))
	  name = './data/%d_%dlabel.png' %  (i, b)
	  io.imsave(name, (label[b][0]*255).astype('uint8'))
	  name = './data/%d_%df32.flo' % (i, b)
	  util.write_flo_file(name, f32[b])
	  name = './data/%d_%df4.flo' % (i, b)
	  util.write_flo_file(name, f4[b])
