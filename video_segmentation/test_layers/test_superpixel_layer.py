import caffe
import numpy as np
from matplotlib.pyplot import imshow, show, figure
from skimage import io
import util
import time

new_path = 'test_superpixel_layer.prototxt'    
new_net = caffe.Net(new_path, caffe.TRAIN)
mean = np.array((0.40787054, 0.45752458, 0.48109378))[::-1].reshape((1, 1, 1, 3)) * 255
for i in range(20):
  print 'iteration', i 
  if (i % 25) == 0:
      time.sleep(1)
  new_net.forward()
  out = new_net.blobs['out'].data #.transpose((0, 2, 3, 1))
  next_im = new_net.blobs['next_im'].data.transpose((0, 2, 3, 1))[:, :, :, ::-1] + mean
  label = new_net.blobs['label'].data

  for b in range(out.shape[0]):
          print 'out[b].shape', out[b].shape
          print 'label[b].shape', label[b].shape
          name = './sdata/%d_%dout.png' % (i,b)
          io.imsave(name, (out[b][0]*255).astype('uint8'))
	  name = './sdata/%d_%dnext_im.png' % (i, b)
	  io.imsave(name, (next_im[b]).astype('uint8'))
	  name = './sdata/%d_%dlabel.png' %  (i, b)
	  io.imsave(name, (label[b][0]*255).astype('uint8'))
